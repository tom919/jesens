<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'guest';

    protected $fillable = [
        'id',
        'name',
        'address',
        'city',
        'country',
        'phone',
        'email',
        'citizenship',
        'status',
    ];
}
