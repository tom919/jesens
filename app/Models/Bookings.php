<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bookings extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'bookings';

    protected $fillable = [
        'id',
        'booking_code',//auto by generator
        'guest',
        'category',
        'booking_date',
        'booking_time',
        'start',
        'end',
        'checkin_time',
        'checkout_time',
        'room_count',
        'status',
        'staff_id'
    ];
}
