<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoomType extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = 'room_type';

    protected $fillable = [
        'id',
        'name',
        'description',
        'low_season_price',
        'high_season_price',
        'image1',
        'image2',
        'image3',
        'color',
    ];
}
