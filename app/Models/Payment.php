<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'payment';

    protected $fillable = [
        'payment_id',
        'booking_id',
        'payment_amount',
        'payment_status',
        'payment_insufficient',
        'acc_destination',
        'method',
        'staff_id'
    ];
}
