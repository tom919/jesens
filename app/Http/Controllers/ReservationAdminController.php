<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RoomType;
use App\Models\Room;
use App\Models\Bookings;
use App\Models\RoomBooking;
use App\Models\Payment;
use App\Models\Guest;
use App\Http\Controllers\UtilController;
use App\Http\Controllers\RoomController;
use DB;
use Auth;

class ReservationAdminController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function Index()
    {
        $data = Bookings::all();

        $data = DB::SELECT("SELECT bookings.id AS booking_id, bookings.booking_code AS booking_code, bookings.category AS booking_category, bookings.booking_date AS booking_date, bookings.start AS booking_start, bookings.status AS booking_status, guest.name AS guest_name FROM bookings JOIN guest ON guest.id=bookings.guest ORDER BY bookings.booking_date ASC LIMIT 500 ");

        return view('admin/reservation')->with('data',$data);

    }

    public function SearchResv(Request $request)
    {
        $field = $request->field;
        $val = $request->val;


        $data = DB::SELECT("SELECT 
                bookings.id AS booking_id, 
                bookings.booking_code AS booking_code,
                bookings.category AS booking_category, 
                bookings.booking_date AS booking_date, 
                bookings.start AS booking_start, 
                bookings.status AS booking_status, 
                guest.name AS guest_name 
                FROM bookings JOIN guest ON guest.id=bookings.guest 
               WHERE ".$field." LIKE '%".$val."%' ORDER BY bookings.booking_date ASC");
        return view('admin/resvres')->with('data',$data);
    }

    public function New()
    {
        return view('admin/reservationnew');
    }
    public function SearchRoom(Request $request)
    {


        $inTime = '12:00:00'; 
        $outTime= '13:00:00';
        $startDate = $request->start_date;
        $endDate = $request->end_date;
        $class = $request->class;
        // room searching
        $res = DB::SELECT("SELECT * FROM room WHERE id NOT IN(
            SELECT room_id FROM room_booking 
            JOIN bookings ON bookings.id = room_booking.booking_id
            WHERE (bookings.end >= '".$startDate."' AND bookings.start <= '".$endDate."'))
            AND room.status = '1'");

        
        $rooms = [];

        foreach($res as $data)
        {
            array_push($rooms, $data->id);
        }
        
            //room choose

        return view('admin/reservationroom')
        ->with('rooms',$rooms)
        ->with('startDate',$startDate)
        ->with('endDate',$endDate);
    }
    public function CompleteResvCheckGuest($id,$startDate,$endDate)
    {
        $data = [$id, $startDate, $endDate];

        return view('admin/reservationcheckguest')->with('data',$data);
    }
    public function SearchGuest(Request $request)
    {
        
        try{
            
            $guest = Guest::where('phone',$request->guest_phone)->get();
            $data = [
                "roomId" => $request->room_id,
                "startDate" => $request->start_date,
                "endDate" => $request->end_date
            ];



            $dates = $this->getBetweenDates($request->start_date, $request->end_date);

            $dateLength = count($dates);
    
            $totalRate = 0;
    
            for($i=0;$i<$dateLength;$i++)
            {
                $totalRate = $totalRate + $this->GetRateDaily($request->room_id, $dates[$i]);
            }
        
        } catch (\Illuminate\Database\QueryException $exception) {
                        
                    $errorInfo = $exception->errorInfo[2];
                    return redirect()->back()->with('error', $errorInfo);
                    
        }

    
        //if guest exists
        if(count($guest) != null)
        {
            return view('admin/resvexguest')->with('guest',$guest[0])->with('data',$data)->with('total',$totalRate);

        }else{
            //if not exsist
          $errorInfo = 'Guest didn\'t exsist';
            return redirect()->back()->with('error', $errorInfo);
        }

    
    }
    public function NewGuest($roomId, $startDate, $endDate)
    {
        $data = [
            "startDate"=>$startDate, 
            "endDate"=>$endDate, 
            "roomId"=>$roomId
        ];

        // $rate = $this->GetRateDaily($roomId, $startDate);

        $dates = $this->getBetweenDates($startDate, $endDate);

        $dateLength = count($dates);

        $totalRate = 0;

        for($i=0;$i<$dateLength;$i++)
        {
            $totalRate = $totalRate + $this->GetRateDaily($roomId, $dates[$i]);
        }


        // return response()->json($totalRate);
        return view('admin/resvnewguest')->with('data',$data)->with('total',$totalRate);
    }
    public function GetRateDaily($roomId, $bookDate)
    {
        $rate = 0;

        $roomType = DB::Select("SELECT * FROM room_type JOIN room ON room_type.id = room.class WHERE room.id = '".$roomId."' "); 



        $day = date("l",strtotime($bookDate));
        if($day == "Saturday" || $day == "Sunday")
        {
            $rate = $roomType[0]->high_season_price;
        }else{
            $rate = $roomType[0]->low_season_price;
        }


        return $rate;
    }

    function getBetweenDates($startDate, $endDate)

    {

        $rangArray = [];

            

        $startDate = strtotime($startDate);

        $endDate = strtotime($endDate);

             

        for ($currentDate = $startDate; $currentDate <= $endDate; 

                                        $currentDate += (86400)) {

                                                

            $date = date('Y-m-d', $currentDate);

            $rangArray[] = $date;

        }

  

        return $rangArray;

    }
    public function Save(Request $request)
    {
        $this->validate($request, [
            'g_name' => 'required',
            'g_address' => 'required',
            'g_country' => 'required',
            'g_phone' => 'required',
            'g_payment_method' => 'required',
            'g_nationality' => 'required',
            'g_category'=> 'required'
        ],
        [
            'g_name.required' => 'Guest Name required',
            'g_address.required' => 'Guest Address required',
            'g_country.required' => 'Guest Country required',
            'g_phone.required' => 'Guest Phone required',
            'g_payment_method.required' => 'Guest Payment required',
            'g_nationality.required' => 'Guest Nationality required',
            'g_category.required'=> 'Category required'
        ]
        );

        //data parsing

        $bStartDate = $request->startDate.' 13:00:00';
        $bEndDate = $request->endDate.' 12:00:00';
        $roomId = $request->roomId;
        $gName = $request->g_name;
        $gAddress = $request->g_address;
        $gCity = $request->g_city;
        $gCountry = $request->g_country;
        $gPhone = $request->g_phone;
        $gEmail = $request->g_email;
        $gNationality = $request->g_nationality;
        $gPayment = $request->g_payment_method;
        $bCategory = $request->g_category;
        $bCheckIn = $request->g_checkin_time;
        $BCI = strtotime( $bCheckIn );
        $fCheckin = date( 'H:i:s', $BCI );
        $BCO = strtotime( '12:00:00' );
        $fCheckout = date( 'H:i:s', $BCO );

        //get user id for staff id

        $staffId = Auth::id();

        //total payment
        $dates = $this->getBetweenDates($bStartDate, $bEndDate);

        $dateLength = count($dates);

        $totalRate = 0;

        for($i=0;$i<$dateLength;$i++)
        {
            $totalRate = $totalRate + $this->GetRateDaily($roomId, $dates[$i]);
        }

        //payment status and insuficient
        $gPayStat = $request->g_payment_status;
        $gPayInsuf = $request->g_payment_insuf;

        //bookings code
        $bookCode = rand(1000, 9999)."-".substr(str_shuffle("ABCDEFGHIJKLMOPQRSTUVWXYZ"), 0, 4);


        //insert to guest
        
        try{

            $resGuest = Guest::create([
                'name'=>$gName,
                'address' => $gAddress,
                'city'=> $gCity,
                'country' => $gCountry,
                'phone' => $gPhone,
                'email' => $gEmail,
                'citizenship' => $gNationality,
                'status'=> 1,
            ])->id;

        } catch (\Illuminate\Database\QueryException $exception) {
                // You can check get the details of the error using `errorInfo`:
                $errorInfo = $exception->errorInfo[2];
                return redirect()->back()->with('error', $errorInfo);
                // Return the response to the client..
        }


        //insert to booking

        try{

            $resBook = Bookings::create([
                'booking_code'=> $bookCode,//auto by generator
                'guest'=> $resGuest,
                'category'=> $bCategory,
                'booking_date'=>date("Y-m-d"),
                'booking_time'=>date("H:i:s"),
                'start'=> $bStartDate,
                'end'=> $bEndDate,
                'checkin_time' => $fCheckin,
                'checkout_time' => $fCheckout,
                'room_count'=>1,
                'status' => 'CONFIRMED',
                'staff_id'=>$staffId,
            ])->id;

        } catch (\Illuminate\Database\QueryException $exception) {
                // You can check get the details of the error using `errorInfo`:
                $errorInfo = $exception->errorInfo[2];
                return redirect()->back()->with('error', $errorInfo);
                // Return the response to the client..
        }

        //insert to room booking

        try{

            $resGuest = RoomBooking::create([
                'room_id'=>$roomId,
                'booking_id' => $resBook,
            ])->id;

        } catch (\Illuminate\Database\QueryException $exception) {
                // You can check get the details of the error using `errorInfo`:
                $errorInfo = $exception->errorInfo[2];
                return redirect()->back()->with('error', $errorInfo);
                // Return the response to the client..
        }

        //insert to payment
        $accDestination = '00000000';
        try{

            $resPayment = Payment::create([
                'booking_id'=>$resBook,
                'payment_amount'=>$totalRate,
                'payment_status'=>$gPayStat,
                'payment_insufficient'=>$gPayInsuf,
                'acc_destination'=>$accDestination,
                'method' => $gPayment,
                'staff_id' => $staffId,
            ])->id;

        } catch (\Illuminate\Database\QueryException $exception) {
                // You can check get the details of the error using `errorInfo`:
                $errorInfo = $exception->errorInfo[2];
                return redirect()->back()->with('error', $errorInfo);
                // Return the response to the client..
        }


        $id=$resBook;


        $data = DB::SELECT("SELECT 
        bookings.id AS booking_id, 
        bookings.booking_code AS booking_code,
        bookings.category AS booking_category, 
        bookings.booking_date AS booking_date, 
        bookings.start AS booking_start,
        bookings.end AS booking_end, 
        bookings.status AS booking_status, 
        guest.name AS guest_name,
        guest.phone AS guest_phone,
        guest.address AS guest_address,
        guest.city AS guest_city,
        guest.country AS guest_country, 
        guest.email AS guest_email,
        guest.citizenship AS guest_citizenship,
        payment.payment_id AS payment_id,
        payment.payment_amount AS payment_amount,
        payment.payment_insufficient AS payment_insuficient,
        payment.payment_status AS payment_status,
        payment.method AS payment_method,
        payment.acc_destination AS payment_destination,
        room_booking.room_id AS room_id,
        room_type.name AS room_type
        FROM bookings 
        JOIN guest ON guest.id=bookings.guest
        JOIN room_booking ON room_booking.booking_id=bookings.id 
        JOIN room ON room.id=room_booking.room_id
        JOIN room_type ON room_type.id=room.class   
        JOIN payment ON payment.booking_id = bookings.id
       WHERE bookings.id=".$id." ORDER BY bookings.booking_date ASC");

        // response data
       $responseData = [
        'bookingId' => $data[0]->booking_id,
        'bookingCode' => $data[0]->booking_code,
        'bookingDate' => $data[0]->booking_date,
        'startDate' => $data[0]->booking_start,
        'endDate' => $data[0]->booking_end,
        'guestName' => $data[0]->guest_name,
        'guestPhone' => $data[0]->guest_phone,
        'guestAddress' => $data[0]->guest_address,
        'guestCity' => $data[0]->guest_city,
        'guestCountry' => $data[0]->guest_country, 
        'guestEmail' => $data[0]->guest_email,
        'guestCitizenship' => $data[0]->guest_citizenship,
        'paymentId' => $data[0]->payment_id,
        'paymentAmount'=>$data[0]->payment_amount,
        'paymentInsuficient' => $data[0]->payment_insuficient,
        'paymentStatus' => $data[0]->payment_status,
        'paymentMethod' => $data[0]->payment_method,
        'paymentAccDest' => $data[0]->payment_destination,
        'roomId' => $data[0]->room_id,
        'roomType' => $data[0]->room_type
    ];



                //send mail notif
                $title = "Booking Notification";
                $receiver = "sandiyudha919@yahoo.co.id";
                $body = "you have new bookings : ".$data[0]->booking_code." from : ".$data[0]->guest_name. " for : ".$data[0]->booking_start;
                $res = $utilController->SendMail($title,$body,$receiver);


        return view('admin/resvdetail')->with('data',$responseData);
    }

    public function SaveExistingGuest(Request $request)
    {
        $this->validate($request, [
            'g_payment_method' => 'required',
            'g_category'=> 'required'
        ],
        [
            'g_payment_method.required' => 'Guest Payment required',
            'g_category.required'=> 'Category required'
        ]
        );

        //data parsing

        $bStartDate = $request->startDate.' 13:00:00';
        $bEndDate = $request->endDate.' 12:00:00';
        $roomId = $request->roomId;
        $guestId = $request->guestId;
        $gPayment = $request->g_payment_method;
        $bCategory = $request->g_category;
        $bCheckIn = $request->g_checkin_time;
        $BCI = strtotime( $bCheckIn );
        $fCheckin = date( 'H:i:s', $BCI );
        // $BCO = strtotime( '12:00:00' );
        $fCheckout = "12:00:00";

        //get user id for staff id

        $staffId = Auth::id();

        //total payment
        $dates = $this->getBetweenDates($bStartDate, $bEndDate);

        $dateLength = count($dates);

        $totalRate = 0;

        for($i=0;$i<$dateLength;$i++)
        {
            $totalRate = $totalRate + $this->GetRateDaily($roomId, $dates[$i]);
        }

        //payment status and insuficient
        $gPayStat = $request->g_payment_status;
        $gPayInsuf = $request->g_payment_insuf;

        //bookings code
        $bookCode = rand(1000, 9999)."-".substr(str_shuffle("ABCDEFGHIJKLMOPQRSTUVWXYZ"), 0, 4);



        //insert to booking

        try{

            $resBook = Bookings::create([
                'booking_code'=> $bookCode,//auto by generator
                'guest'=> $guestId,
                'category'=> $bCategory,
                'booking_date'=>date("Y-m-d"),
                'booking_time'=>date("H:i:s"),
                'start'=> $bStartDate,
                'end'=> $bEndDate,
                'checkin_time' => $fCheckin,
                'checkout_time' => $fCheckout,
                'room_count'=>1,
                'status' => 'CONFIRMED',
                'staff_id'=>$staffId,
            ])->id;

        } catch (\Illuminate\Database\QueryException $exception) {
                // You can check get the details of the error using `errorInfo`:
                $errorInfo = $exception->errorInfo[2];
                return response()->json($errorInfo);
                // return redirect()->back()->with('error', $errorInfo);
                // Return the response to the client..
        }

        //insert to room booking

        try{

            $resGuest = RoomBooking::create([
                'room_id'=>$roomId,
                'booking_id' => $resBook,
            ])->id;

        } catch (\Illuminate\Database\QueryException $exception) {
                // You can check get the details of the error using `errorInfo`:
                $errorInfo = $exception->errorInfo[2];
                return response()->json($errorInfo);
                // return redirect()->back()->with('error', $errorInfo);
                // Return the response to the client..
        }

        //insert to payment
        $accDestination = '00000000';
        try{

            $resPayment = Payment::create([
                'booking_id'=>$resBook,
                'payment_amount'=>$totalRate,
                'payment_status'=>$gPayStat,
                'payment_insufficient'=>$gPayInsuf,
                'acc_destination'=>$accDestination,
                'method' => $gPayment,
                'staff_id' => $staffId,
            ])->id;

        } catch (\Illuminate\Database\QueryException $exception) {
                // You can check get the details of the error using `errorInfo`:
                $errorInfo = $exception->errorInfo[2];
                return redirect()->back()->with('error', $errorInfo);
                // Return the response to the client..
        }



        $id=$resBook;


        $data = DB::SELECT("SELECT 
        bookings.id AS booking_id, 
        bookings.booking_code AS booking_code,
        bookings.category AS booking_category, 
        bookings.booking_date AS booking_date, 
        bookings.start AS booking_start,
        bookings.end AS booking_end, 
        bookings.status AS booking_status, 
        guest.name AS guest_name,
        guest.phone AS guest_phone,
        guest.address AS guest_address,
        guest.city AS guest_city,
        guest.country AS guest_country, 
        guest.email AS guest_email,
        guest.citizenship AS guest_citizenship,
        payment.payment_id AS payment_id,
        payment.payment_amount AS payment_amount,
        payment.payment_insufficient AS payment_insuficient,
        payment.payment_status AS payment_status,
        payment.method AS payment_method,
        payment.acc_destination AS payment_destination,
        room_booking.room_id AS room_id,
        room_type.name AS room_type
        FROM bookings 
        JOIN guest ON guest.id=bookings.guest
        JOIN room_booking ON room_booking.booking_id=bookings.id 
        JOIN room ON room.id=room_booking.room_id
        JOIN room_type ON room_type.id=room.class   
        JOIN payment ON payment.booking_id = bookings.id
       WHERE bookings.id=".$id." ORDER BY bookings.booking_date ASC");


        $lastIndex = count($data)-1;

        // response data
       $responseData = [
        'bookingId' => $data[$lastIndex]->booking_id,
        'bookingCode' => $data[$lastIndex]->booking_code,
        'bookingDate' => $data[$lastIndex]->booking_date,
        'startDate' => $data[$lastIndex]->booking_start,
        'endDate' => $data[$lastIndex]->booking_end,
        'guestName' => $data[$lastIndex]->guest_name,
        'guestPhone' => $data[$lastIndex]->guest_phone,
        'guestAddress' => $data[$lastIndex]->guest_address,
        'guestCity' => $data[$lastIndex]->guest_city,
        'guestCountry' => $data[$lastIndex]->guest_country, 
        'guestEmail' => $data[$lastIndex]->guest_email,
        'guestCitizenship' => $data[$lastIndex]->guest_citizenship,
        'paymentId' => $data[$lastIndex]->payment_id,
        'paymentAmount'=>$data[$lastIndex]->payment_amount,
        'paymentInsuficient' => $data[$lastIndex]->payment_insuficient,
        'paymentStatus' => $data[$lastIndex]->payment_status,
        'paymentMethod' => $data[$lastIndex]->payment_method,
        'paymentAccDest' => $data[$lastIndex]->payment_destination,
        'roomId' => $data[$lastIndex]->room_id,
        'roomType' => $data[$lastIndex]->room_type
    ];



                //send mail notif
                $utilController = new UtilController();


                $title = "Booking Notification";
                $receiver = "sandiyudha919@yahoo.co.id";
                $body = "you have new bookings : ".$data[$lastIndex]->booking_code." from : ".$data[$lastIndex]->guest_name. " for : ".$data[$lastIndex]->booking_start;
                $res = $utilController->SendMail($title,$body,$receiver);


        return view('admin/resvdetail')->with('data',$responseData);
    }
    public function Detail($id){

        $data = DB::SELECT("SELECT 
        bookings.id AS booking_id, 
        bookings.booking_code AS booking_code,
        bookings.category AS booking_category, 
        bookings.booking_date AS booking_date, 
        bookings.start AS booking_start,
        bookings.end AS booking_end, 
        bookings.status AS booking_status, 
        guest.name AS guest_name,
        guest.phone AS guest_phone,
        guest.address AS guest_address,
        guest.city AS guest_city,
        guest.country AS guest_country, 
        guest.email AS guest_email,
        guest.citizenship AS guest_citizenship,
        payment.payment_id AS payment_id,
        payment.payment_amount AS payment_amount,
        payment.payment_insufficient AS payment_insuficient,
        payment.payment_status AS payment_status,
        payment.method AS payment_method,
        payment.acc_destination AS payment_destination,
        room_booking.room_id AS room_id,
        room_type.name AS room_type
        FROM bookings 
        JOIN guest ON guest.id=bookings.guest
        JOIN room_booking ON room_booking.booking_id=bookings.id 
        JOIN room ON room.id=room_booking.room_id
        JOIN room_type ON room_type.id=room.class   
        JOIN payment ON payment.booking_id = bookings.id
       WHERE bookings.id=".$id." ORDER BY bookings.booking_date ASC");


       // response data
       $responseData = [
        'bookingId' => $data[0]->booking_id,
        'bookingCode' => $data[0]->booking_code,
        'bookingDate' => $data[0]->booking_date,
        'startDate' => $data[0]->booking_start,
        'endDate' => $data[0]->booking_end,
        'guestName' => $data[0]->guest_name,
        'guestPhone' => $data[0]->guest_phone,
        'guestAddress' => $data[0]->guest_address,
        'guestCity' => $data[0]->guest_city,
        'guestCountry' => $data[0]->guest_country, 
        'guestEmail' => $data[0]->guest_email,
        'guestCitizenship' => $data[0]->guest_citizenship,
        'paymentId' => $data[0]->payment_id,
        'paymentAmount'=>$data[0]->payment_amount,
        'paymentInsuficient' => $data[0]->payment_insuficient,
        'paymentStatus' => $data[0]->payment_status,
        'paymentMethod' => $data[0]->payment_method,
        'paymentAccDest' => $data[0]->payment_destination,
        'roomId' => $data[0]->room_id,
        'roomType' => $data[0]->room_type
    ];


    return view('admin/resvdetail')->with('data',$responseData);

    }
    public function Edit($id)
    {

        $data = DB::SELECT("SELECT 
        bookings.id AS booking_id, 
        bookings.booking_code AS booking_code,
        bookings.category AS booking_category, 
        bookings.booking_date AS booking_date, 
        bookings.start AS booking_start,
        bookings.end AS booking_end, 
        bookings.status AS booking_status,
        guest.id AS guest_id, 
        guest.name AS guest_name,
        guest.phone AS guest_phone,
        guest.address AS guest_address,
        guest.city AS guest_city,
        guest.country AS guest_country, 
        guest.email AS guest_email,
        guest.citizenship AS guest_citizenship,
        payment.payment_id AS payment_id,
        payment.payment_amount AS payment_amount,
        payment.payment_insufficient AS payment_insuficient,
        payment.payment_status AS payment_status,
        payment.method AS payment_method,
        payment.acc_destination AS payment_destination,
        room_booking.id AS roombooking_id,
        room_booking.room_id AS room_id,
        room_type.name AS room_type
        FROM bookings 
        JOIN guest ON guest.id=bookings.guest
        JOIN room_booking ON room_booking.booking_id=bookings.id 
        JOIN room ON room.id=room_booking.room_id
        JOIN room_type ON room_type.id=room.class   
        JOIN payment ON payment.booking_id = bookings.id
       WHERE bookings.id=".$id." ORDER BY bookings.booking_date ASC");


       // response data
       $responseData = [
        'bookingId' => $data[0]->booking_id,
        'bookingCode' => $data[0]->booking_code,
        'bookingDate' => $data[0]->booking_date,
        'bookingCategory' => $data[0]->booking_category,
        'bookingStatus' => $data[0]->booking_status,
        'startDate' => $data[0]->booking_start,
        'endDate' => $data[0]->booking_end,
        'guestId' => $data[0]->guest_id,
        'guestName' => $data[0]->guest_name,
        'guestPhone' => $data[0]->guest_phone,
        'guestAddress' => $data[0]->guest_address,
        'guestCity' => $data[0]->guest_city,
        'guestCountry' => $data[0]->guest_country, 
        'guestEmail' => $data[0]->guest_email,
        'guestNationality' => $data[0]->guest_citizenship,
        'paymentId' => $data[0]->payment_id,
        'paymentAmount'=>(int)$data[0]->payment_amount,
        'paymentInsuficient' => $data[0]->payment_insuficient,
        'paymentStatus' => $data[0]->payment_status,
        'paymentMethod' => $data[0]->payment_method,
        'paymentAccDest' => $data[0]->payment_destination,
        'rbId' => $data[0]->roombooking_id,
        'roomId' => $data[0]->room_id,
        'roomType' => $data[0]->room_type
    ];




        return view('admin/resvedit')->with('data',$responseData);;

    }
    public function Update(Request $request)
    {
        $this->validate($request, 
            [
                'bookingDate' => 'required',
                'bookingCategory'=> 'required',
                'startDate' => 'required',
                'endDate' => 'required',
                'guestName' => 'required',
                'guestAddress' => 'required',
                'guestCity' => 'required',
                'guestPhone'=> 'required',
                'guestNationality' => 'required',
                'paymentAmount' => 'required',
                'paymentMethod' => 'required',
                'paymentStatus' => 'required',
                'roomId' => 'required',
            ],
        
        );

        $bookingId = $request->bookingId;
        $bookingDate = $request->bookingDate;
        $bookingCategory = $request->bookingCategory;
        $bookingStatus = $request->bookingStatus;
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $guestId = $request->guestId;
        $guestName = $request->guestName;
        $guestPhone = $request->guestPhone;
        $guestAddress = $request->guestAddress;
        $guestCity = $request->guestCity;
        $guestCountry = $request->guestCountry; 
        $guestEmail = $request->guestEmail;
        $guestNationality = $request->guestNationality;
        $paymentId = $request->paymentId;
        $paymentAmount = $request->paymentAmount;
        $paymentInsuficient = $request->paymentInsuficient;
        $paymentStatus = $request->paymentStatus;
        $paymentMethod = $request->paymentMethod;
        $roomId = $request->roomId;
        $rbId = $request->rbId;


        //query booking

        $data = [
            'category'=>$bookingCategory,
            'start' => $startDate,
            'end' => $endDate,
            'status'=>$bookingStatus
        ];

        $data = array_filter($data,"strlen");

        // return response()->json($data);
        
        try{


            $res = Bookings::where('id', $bookingId)->update($data);


        } catch (\Illuminate\Database\QueryException $exception) {
                // You can check get the details of the error using `errorInfo`:
                $errorInfo = $exception->errorInfo[2];
                return redirect()->back()->with('error', $errorInfo);
                // Return the response to the client..
        }
        //query guest
        $guestData = [
            'name'=>$guestName,
            'address'=>$guestAddress,
            'city'=>$guestCity,
            'country'=>$guestCountry,
            'phone'=>$guestPhone,
            'email'=>$guestEmail,
            'citizenship'=>$guestNationality
        ];

        $data = array_filter($data,"strlen");

        // return response()->json($data);
        
        try{


            $res = Guest::where('id', $guestId)->update($guestData);


        } catch (\Illuminate\Database\QueryException $exception) {
                // You can check get the details of the error using `errorInfo`:
                $errorInfo = $exception->errorInfo[2];
                return redirect()->back()->with('error', "Guest : ".$errorInfo);
                // Return the response to the client..
        }

        //query payment
        $paymentData = [
            'payment_amount'=>$paymentAmount,
            'payment_insufficient'=>$paymentInsuficient,
            'payment_status'=>$paymentStatus,
            'method'=>$paymentMethod
        ];

        $data = array_filter($data,"strlen");

        // return response()->json($data);
        
        try{


            $res = Payment::where('payment_id', $paymentId)->update($paymentData);


        } catch (\Illuminate\Database\QueryException $exception) {
                // You can check get the details of the error using `errorInfo`:
                $errorInfo = $exception->errorInfo[2];
                return redirect()->back()->with('error', "payment : ".$errorInfo);
                // Return the response to the client..
        }


        //query room booking

        $rbData = [
            'room_id'=>$roomId
        ];

        $data = array_filter($data,"strlen");

        // return response()->json($data);
        
        try{

            $res = RoomBooking::where('id', $rbId)->update($rbData);

        } catch (\Illuminate\Database\QueryException $exception) {
                // You can check get the details of the error using `errorInfo`:
                $errorInfo = $exception->errorInfo[2];
                return redirect()->back()->with('error', "room book :".$errorInfo);
                // Return the response to the client..
        }

        return  redirect('/reservationadmin')->with('response',"Updated");

    }
    public function Delete($id)
    {

    }
}
