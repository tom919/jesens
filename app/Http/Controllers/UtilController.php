<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class UtilController extends Controller
{
    //
    public static function MaskString($string){
    
        $mask_string =  str_repeat("*", strlen($string)-4) . substr($string, -4);
        
        return $mask_string;
    }

    public function UploadFile($foto, $fileName)
    {
        $uploadPath = 'img/upload';
        $foto->move($uploadPath,$fileName);
    }
    public function DeleteFile($fileName)
    {
        $imgPath = 'img/upload';
        try{
            unlink($imgPath.'/'.$fileName);
        }catch(Exception $e){
            return $e->getMessage();
        }

        return 'success';
    }
    public function SendMail($title, $body, $receiver)
    {


        require base_path("vendor/autoload.php");
        $mail = new PHPMailer(true);     // Passing `true` enables exceptions

        try {

            // Email server settings
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host = 'mail.jesens-inn.com ';             //  smtp host
            $mail->SMTPAuth = true;
            $mail->Username = 'system@jesens-inn.com';   //  sender username
            $mail->Password = 'A4BxO2q6IpZa';       // sender password
            $mail->SMTPSecure = 'ssl';                  // encryption - ssl/tls
            $mail->Port = 465;                          // port - 587/465

            $mail->setFrom('system@jesens-inn.com', $title);
            $mail->addAddress($receiver);


    


            $mail->isHTML(true);                // Set email content format to HTML

            $mail->Subject = $title;
            $mail->Body    = $body;

            // $mail->AltBody = plain text version of email body;

            if( !$mail->send() ) {
                return  "Email not sent.";
            }
            
            else {
                return  "Email has been sent.";
            }

        } catch (Exception $e) {
             return 'Message could not be sent :'.$e;;
        }
    }
    
}
