<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RoomType;
use App\Models\Image;
use App\Http\Controllers\UtilController;

class ImageAdminController extends Controller
{
    //

    public function Index()
    {
        $images = Image::all();
        return view('admin/image')->with('images',$images);
    }

    public function New()
    {
        return view('admin/imagenew');
    }
    public function Save(Request $request)
    {
        $utilController = new UtilController();

        $this->validate($request, [
            'name' => 'required',
            'category' => 'required',
            'status' => 'required',
            'imageFile' => 'required',
		],
        [
            'name.required' => 'required',
            'category.required' => 'required',
            'status.required' => 'required',
            'imageFile.required' => 'required',
        ]
        );

        //main
        $name = $request->name; 
        $category = $request->category;
        $status = $request->status;
        if($request->roomRef != null)
        {
            $roomRefId = $request->roomRef;
        }else{
            $roomRefId ="";
        }
   

        //image
        $namePrefix = preg_replace('/\s+/', '',$name);
        $imgFile = ($request->hasFile('imageFile'))?$request->file('imageFile'): null;
        $imgName = ($imgFile)?$namePrefix."-".$category."-".rand(1000,9999).".".$imgFile->getClientOriginalExtension():"No File";
      



        try{

            $res = Image::create([
                'name'=>$imgName,
                'category'=>$category,
                'room_type'=>$roomRefId,
                'status'=>$status,
            ]);

        } catch (\Illuminate\Database\QueryException $exception) {
                // You can check get the details of the error using `errorInfo`:
                $errorInfo = $exception->errorInfo[2];
                return redirect('/imageadmin/new')->with('error', $errorInfo);
                // Return the response to the client..
        }

        //upload image opration

        if($imgFile != null ){
            $utilController->UploadFile($imgFile,$imgName);
        }


        

        // return response()->json($data);
        return redirect('/imageadmin');
    }
    public function Edit($id)
    {
        $img = Image::find($id);
        return view('admin/imageedit')->with('images', $img);

    }
    public function Update(Request $request)
    {
        $utilController = new UtilController();

        $this->validate($request, [
            'name' => 'required',
            'category' => 'required',
            'status' => 'required',

		],
        [
            'name.required' => 'required',
            'category.required' => 'required',
            'status.required' => 'required',

        ]
        );

           //main
        $id = $request->imagesId;
        $name = $request->name; 
        $category = $request->category;
        $status = $request->status;
        if($request->roomRef != null)
        {
            $roomRefId = $request->roomRef;
        }else{
            $roomRefId ="";
        }
   

        //image
        // $namePrefix = preg_replace('/\s+/', '',$name);
        $imgFile = ($request->hasFile('imageFile'))?$request->file('imageFile'): null;
        // $imgName = ($imgFile)?$namePrefix."-".$category."-".rand(1000,9999).".".$imgFile->getClientOriginalExtension():"No File";
      
         
        $data = [
            'name'=>$name,
            'category'=>$category,
            'room_type'=>$roomRefId,
            'status'=>$status,
        ];

        $data = array_filter($data,"strlen");

        // return response()->json($data);
        
        try{


            $res = Image::where('id', $id)->update($data);


        } catch (\Illuminate\Database\QueryException $exception) {
                // You can check get the details of the error using `errorInfo`:
                $errorInfo = $exception->errorInfo[2];
                return redirect()->back()->with('error', $errorInfo);
                // Return the response to the client..
        }


        //upload image operation
        // need unlink operation

        if($imgFile != null ){
            //delete if old file exsist
            $oldImgName = $request->name;
            if($oldImgName != null)
            {
                $utilController->DeleteFile($request->name);
            }
            
            //then upload
            $utilController->UploadFile($imgFile,$imgName);
        }


        return redirect('/imageadmin');


    }

    public function Delete($id)
    {
        $utilController = new UtilController();
        
        try{

            $img = Image::find($id);

        } catch (\Illuminate\Database\QueryException $exception) {
             
                $errorInfo = $exception->errorInfo[2];
                return $errorInfo;
                
        }


        // return response()->json($prop);

        //delete image
        if($img->name != "No File" ){
                $utilController->DeleteFile($img->name);

        }
           

        //execute delete from db
        try{
            $res = $img->delete();

        } catch (\Illuminate\Database\QueryException $exception) {
             
                $errorInfo = $exception->errorInfo[2];
                return redirect('/imageadmin')->with('error', $errorInfo);
                
        } 
        return redirect('/imageadmin');

    }
}
