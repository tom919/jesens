<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RoomType;
use App\Models\Room;
use App\Http\Controllers\UtilController;


class RoomController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    //room
    public function Index(){
        return view('admin/room');
    }
    public function New()
    {
        return view('admin/roomnew');
    }
    public function Save(Request $request)
    {
        $utilController = new UtilController();  

        $this->validate($request, [
            'id' => 'required',
            'class' => 'required',
			'building' => 'required',
            'floor' => 'required',
			'status' => 'required',
		],
        [
            'id.required' => 'Room No Required',
            'class.required' => 'Room Type / Class Required',
            'building.required' => 'Building Required',
            'floor.required' => 'Floor Required',
            'status.required' => 'Status Required',
        ]
    );
             
        
        try{

            $res = Room::create([
                'id'=> $request->id,
                'class'=>$request->class,
                'building'=> $request->building,
                'floor' => $request->floor,
                'status' => $request->status
            ]);
         

        } catch (\Illuminate\Database\QueryException $exception) {
                $errorMessage = $exception->errorInfo[2];

                if($exception->errorInfo[1] == 1062)
                {
                    return redirect('roomadmin/new')->with('error', "Room No already exists");
                }else{
                    return redirect('roomadmin/new')->with('error', $errorMessage);
                }
               
        }


        // $utilController->UploadFile($foto,$fileName);

        return redirect('roomadmin')->with('response','Success'); 
    }

    public function Edit($id)
    {
        $room = Room::find($id);

        return view('admin/roomupdate')->with('room',$room);
    }

    public function Update(Request $request)
    {
        try{

            $this->validate($request, [
                'id' => 'required',
                'class' => 'required',
                'building' => 'required',
                'floor' => 'required',
                'status' => 'required',
            ],
            [
                'id' => 'Room No required',
                'class' => 'Room Type / Class required',
                'building' => 'Building required',
                'floor' => 'Floor required',
                'status' => 'Status required',
            ]
        );
            

            $data = [
                'class'=>$request->class,
                'building'=> $request->building,
                'floor' => $request->floor,
                'status' => $request->status
            ];

            $res = Room::where('id', $request->id)->update($data);


        } catch (\Illuminate\Database\QueryException $exception) {

                $errorInfo = $exception->errorInfo[2];
                return redirect()->back()->with('error', $errorInfo);

        }

        return redirect('/roomadmin')->with('response',"Updated");

    }
    public function Delete($id)
    {
        try{

            $room = Room::find($id);
            $room->delete();

        } catch (\Illuminate\Database\QueryException $exception) {
             
                $errorInfo = $exception->errorInfo[2];
                return redirect()->back()->with('error', $errorInfo);
                
        }

        return redirect('/roomadmin')->with('response',"Deleted");
    }

    // room type
    public function RoomType(){
        $rt = RoomType::all();

        return view('admin/roomtype')->with('room_types',$rt);
    }

    public static function GetRoomType()
    {
        return RoomType::all();
    }
    public function NewRoomType(){
        return view('admin/roomtypenew');
    }
    public function SaveRoomType(Request $request){
        $utilController = new UtilController();  

        $this->validate($request, [
            'room_id' => 'required',
            'name' => 'required',
            'description' => 'required',
			'lowPrice' => 'required',
			'highPrice' => 'required',
		],
        [
            'room_id.required' => 'ID Required',
            'name.required' => 'Name Required',
            'description.required' => 'Description Required',
            'lowPrice.required' => 'Low Price Required',
            'highPrice.required' => 'High Price Required',
        ]
    );
        
        $id = $request->room_id;
        $name = $request->name;
        $description = $request->description;
        $lowPrice = $request->lowPrice;
        $highPrice = $request->highPrice;
        
        $namePrefix = preg_replace('/\s+/', '',$name);

        $image1 = ($request->hasFile('image1'))?$request->file('image1'): null;
        $image1n = ($image1)?$namePrefix."-prop-".rand(1000,9999).".".$image1->getClientOriginalExtension():null;
        $image2 = ($request->hasFile('image2'))?$request->file('image2'): null;
        $image2n = ($image2)?$namePrefix."-prop-".rand(1000,9999).".".$image2->getClientOriginalExtension():null;
        $image3 = ($request->hasFile('image3'))?$request->file('image3'): null;
        $image3n = ($image3)?$namePrefix."-prop-".rand(1000,9999).".".$image3->getClientOriginalExtension():null;
        
        $res = "";
        
        try{

            $res = RoomType::create([
                'id'=> $id,
                'name'=>$name,
                'description'=>$description,
                'low_season_price'=> $lowPrice,
                'high_season_price' => $highPrice,
                'image1' => $image1n,
                'image2' => $image2n,
                'image3' => $image3n
            ]);

        
            $last_id = $res->id;
                $res = "success";

         

        } catch (\Illuminate\Database\QueryException $exception) {
                // You can check get the details of the error using `errorInfo`:
                $errorMessage = $exception->errorInfo[2];

                if($exception->errorInfo[1] == 1062)
                {
                    return redirect('room/typenew')->with('error', "Room Id already exists");
                }else{
                    return redirect('room/typenew')->with('error', $errorMessage);
                }
               
                // Return the response to the client..
        }

       
        if($image1 != null ){
            $utilController->UploadFile($image1,$image1n);
        }
        if($image2 != null ){
            $utilController->UploadFile($image2,$image2n);
        }
        if($image3 != null ){
            $utilController->UploadFile($image3,$image3n);
        }

        return redirect('room/type')->with('response',$res);


    }
    public function EditRoomType($id)
    {

        $rt = RoomType::find($id);

        return view('admin/roomtypeupdate')->with('data',$rt);
    }

    public function UpdateRoomType(Request $request)
    {
        try{

            $this->validate($request, [
                'room_id' => 'required',
                'description' => 'required',
                'name' => 'required',
                'lowPrice' => 'required',
                'highPrice' => 'required',
            ],
            [
                'room_id.required' => 'ID Required',
                'name.required' => 'Name Required',
                'description.required' => 'Description Required',
                'lowPrice.required' => 'Low Price Required',
                'highPrice.required' => 'High Price Required',
            ]
        );
            
            $id = $request->room_id;
            $description = $request->description;
            $name = $request->name;
            $lowPrice = $request->lowPrice;
            $highPrice = $request->highPrice;


            $namePrefix = preg_replace('/\s+/', '',$name);

            $image1 = ($request->hasFile('image1'))?$request->file('image1'): null;
            $image1n = ($image1)?$namePrefix."-prop-".rand(1000,9999).".".$image1->getClientOriginalExtension():null;
            $image2 = ($request->hasFile('image2'))?$request->file('image2'): null;
            $image2n = ($image2)?$namePrefix."-prop-".rand(1000,9999).".".$image2->getClientOriginalExtension():null;
            $image3 = ($request->hasFile('image3'))?$request->file('image3'): null;
            $image3n = ($image3)?$namePrefix."-prop-".rand(1000,9999).".".$image3->getClientOriginalExtension():null;

            $data = [
                'name'=>$name,
                'description' => $description,
                'low_season_price'=> $lowPrice,
                'high_season_price' => $highPrice,
                'image1' => $image1n,
                'image2' => $image2n,
                'image3' => $image3n
            ];

            $res = RoomType::where('id', $id)->update($data);


        } catch (\Illuminate\Database\QueryException $exception) {

                $errorInfo = $exception->errorInfo[2];
                return redirect()->back()->with('error', $errorInfo);

        }

        if($image1 != null ){
            //delete if old file exsist
            if($request->image1nOld != null)
            {
                $utilController->DeleteFile($request->image1nOld);
            }
            
            //then upload
            $utilController->UploadFile($image1,$image1n);
        }

        if($image2 != null ){
            //delete if old file exsist
            if($request->image2nOld != null)
            {
                $utilController->DeleteFile($request->image2nOld);
            }
            
            //then upload
            $utilController->UploadFile($image2,$image2n);
        }
        if($image3 != null ){
            //delete if old file exsist
            if($request->image3nOld != null)
            {
                $utilController->DeleteFile($request->image3nOld);
            }
            
            //then upload
            $utilController->UploadFile($image3,$image3n);
        }

        return redirect('room/type')->with('response',"Updated");

    }

    public function DeleteRoomType($id)
    {
        try{

            $rt = RoomType::find($id);
            $rt->delete();

        } catch (\Illuminate\Database\QueryException $exception) {
             
                $errorInfo = $exception->errorInfo[2];
                return redirect()->back()->with('error', $errorInfo);
                
        }

        return redirect('room/type')->with('response',"Deleted");
    }

    public static function GetRoomBF($building, $floor)
    {

        $result  = Room::where('building',$building)
        ->where('floor',$floor)
        ->get();


        return $result;
    }
    public static function RoomClassColor($class, $status)
    {

        $res = RoomType::where('id',$class)->get();

        if($status == '0')
        {
            $color = '#ff033e';
        }else if($status == '88')
        {
            $color = '#808080';
        }
        else{
            $color = $res[0]->color;
        }

        return $color;
    }

    public static function GetRoomClass($id)
    {
        $room = Room::where('id', $id)->get();

        $res = RoomType::where('id',$room[0]->class)->get();

        return $res[0]->name;
    }

    public static function AvailabilitySign($rooms, $room_no, $room_status)
    {

        if(!in_array($room_no, $rooms))
        {
            $stat = '88';

        }else{
            $stat = $room_status;
        }
        return $stat;
    }
    
}
