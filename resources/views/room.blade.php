@extends('layouts.front')

@section('content')


        <!-- About -->
        <section class="row" id="tmAbout">
          <header class="col-12 tm-about-header">
            <h2 class="text-uppercase text-center text-white tm-about-page-title">Our Room</h2>
            <hr class="tm-about-title-hr">
          </header>

          @foreach ($room_types as $rt)
          <div class="col-lg-3 mt-3">
            <div class="tm-bg-black-transparent tm-about-box" style="height: 100%;">
              <a href="{{url('room/detail')}}/{{$rt->id}}"><img src="{{url('img/upload')}}/{{$rt->image1}}" style="max-width: 60%;"></a>      
              <h3 class="tm-about-name">{{$rt->name}}</h3>
              <p>Start From : IDR
                    @php 
                    echo number_format($rt->low_season_price,2,',','.');
                    @endphp  
                    
                    per night</p>
                  
                        <a href="{{url('room/detail')}}/{{$rt->id}}" class="text-center">Detail...</a>
                  <br>
              <a href="{{url('booking')}}/{{$rt->id}}"><button class="btn btn-primary"><i class="fa fa-bell-o" aria-hidden="true"></i>
Search available room</button></a>
              <hr>
            </div>
          </div>
          @endforeach

        </section>

        <!-- App Features -->
        <section id="tmAppFeatures">
            <div class="row">
                <header class="col-12 text-center text-white tm-bg-black-transparent p-5 tm-app-header">
                    <h2 class="text-uppercase mb-3 tm-app-feature-header">Room Facility</h2>
                    <p class="mb-0 small">Suspendisse finibus tellus eget sem lacinia, vel lacinia libero consequat. Sed imperdiet placerat vehicula.</p>
                </header>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="tm-bg-white-transparent tm-app-feature-box">
                        <div class="tm-app-feature-icon-container">
                            <i class="fas fa-3x fa-tv tm-app-feature-icon"></i>
                        </div>
                        <div class="tm-app-feature-description-box">
                            <h3 class="mb-4 tm-app-feature-title">TV Cable</h3>
                            <p class="tm-app-feature-description">Suspendisse finibus tellus eget sem lacinia, vel lacinia libero consequat. Sed imperdiet placerat vehicula. Etiam eu egestas nibh. In turpis ligula.</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="tm-bg-white-transparent tm-app-feature-box">
                        <div class="tm-app-feature-icon-container">
                            <i class="fas fa-3x fa-binoculars tm-app-feature-icon"></i>
                        </div>
                        <div class="tm-app-feature-description-box">
                            <h3 class="mb-4 tm-app-feature-title">Refrigator</h3>
                            <p class="tm-app-feature-description">Etiam fermentum nisi lorem, vel pharetra ipsum egestas non. Proin odio ipsum, vestibulum quis elit quis, vulputate venenatis neque. Nam dignissim libero.</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="tm-bg-white-transparent tm-app-feature-box">
                        <div class="tm-app-feature-icon-container">
                            <i class="fas fa-3x fa-campground tm-app-feature-icon"></i>
                        </div>
                        <div class="tm-app-feature-description-box">
                            <h3 class="mb-4 tm-app-feature-title">Hot Water</h3>
                            <p class="tm-app-feature-description">Aliquam erat volutpat. Phasellus a odio eget enim luctus vestibulum nec sed tellus. Pellentesque aliquam sem quis lobortis laoreet. Donec egestas.</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="tm-bg-white-transparent tm-app-feature-box">
                        <div class="tm-app-feature-icon-container">
                            <i class="fas fa-3x fa-anchor tm-app-feature-icon"></i>
                        </div>
                        <div class="tm-app-feature-description-box">
                            <h3 class="mb-4 tm-app-feature-title">Air Conditioner</h3>
                            <p class="tm-app-feature-description">Donec ut odio nec nisl feugiat dictum. Quisque sit amet mattis dolor. Morbi et erat vestibulum, finibus nunc quis, fermentum eros. Cras laoreet eros.</p>
                        </div>
                    </div>
                </div>
            </div>
          
        </section>
            </div>
            <!-- <div class="tm-home-right">
              <img src="{{ url('template/img/mobile-screen.png') }}" alt="App on Mobile mockup" />
            </div> -->
          </div>
        </section>

        <!-- Features -->
        <!-- <div class="row" id="tmFeatures">
          <div class="col-lg-4">
            <div class="tm-bg-white-transparent tm-feature-box">
            <h3 class="tm-feature-name">High Performance</h3>
            
            <div class="tm-feature-icon-container">
                <i class="fas fa-3x fa-server"></i>
            </div>

            <p class="text-center">Download and use this layout for your sites. Total 5 HTML pages included.</p>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="tm-bg-white-transparent tm-feature-box">
                <h3 class="tm-feature-name">Fast Support</h3>

                <div class="tm-feature-icon-container">
                    <i class="fas fa-3x fa-headphones"></i>
                </div>
                <p class="text-center">You are allowed to use this for commercial purpose or personal site.
                </p>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="tm-bg-white-transparent tm-feature-box">
                <h3 class="tm-feature-name">App Marketing</h3>

                <div class="tm-feature-icon-container">
                    <i class="fas fa-3x fa-satellite-dish"></i>
                </div>
                <p class="text-center">You are NOT allowed to redistribute this template on any download site.
                </p>
            </div>
          </div>
        </div> -->

  <!-- modal -->
  <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-body">
           <!-- carousel -->
          <div
               id='carouselExampleIndicators'
               class='carousel slide'
               data-ride='carousel'
               >
            <ol class='carousel-indicators'>
              <li
                  data-target='#carouselExampleIndicators'
                  data-slide-to='0'
                  class='active'
                  ></li>
              <li
                  data-target='#carouselExampleIndicators'
                  data-slide-to='1'
                  ></li>
              <li
                  data-target='#carouselExampleIndicators'
                  data-slide-to='2'
                  ></li>
            </ol>
            <div class='carousel-inner'>
              <div class='carousel-item active'>
                <img class='img-size' src='https://images.unsplash.com/photo-1485470733090-0aae1788d5af?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1391&q=80' alt='First slide' />
              </div>
              <div class='carousel-item'>
                <img class='img-size' src='https://images.unsplash.com/photo-1491555103944-7c647fd857e6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80' alt='Second slide' />
              </div>
              <div class='carousel-item'>
                <img class='img-size' src='https://images.unsplash.com/photo-1464822759023-fed622ff2c3b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80' alt='Second slide' />
              </div>
            </div>
            <a
               class='carousel-control-prev'
               href='#carouselExampleIndicators'
               role='button'
               data-slide='prev'
               >
              <span class='carousel-control-prev-icon'
                    aria-hidden='true'
                    ></span>
              <span class='sr-only'>Previous</span>
            </a>
            <a
               class='carousel-control-next'
               href='#carouselExampleIndicators'
               role='button'
               data-slide='next'
               >
              <span
                    class='carousel-control-next-icon'
                    aria-hidden='true'
                    ></span>
              <span class='sr-only'>Next</span>
            </a>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Search Now</button>
        </div>
      </div>
    </div>
  </div>

@endsection
