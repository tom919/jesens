<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Jesens Inn Hotel Kuta</title>
     <!-- CSRF Token -->
     <meta name="csrf-token" content="{{ csrf_token() }}">
     <link rel="icon" type="image/x-icon" href="{{ url('img/logo.png')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" />
    <link rel="stylesheet" href="{{ url('template/css/all.min.css') }}" />
    <link rel="stylesheet" href="{{ url('template/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ url('template/css/templatemo-style.css') }}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.css" integrity="sha512-EaaldggZt4DPKMYBa143vxXQqLq5LE29DG/0OoVenoyxDrAScYrcYcHIuxYO9YNTIQMgD8c8gIUU8FQw7WpXSQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <style>
 .img-size{
/* 	padding: 0;
	margin: 0; */
	height: 450px;
	width: 700px;
	background-size: cover;
	overflow: hidden;
}
.modal-content {
   width: 700px;
  border:none;
}
.modal-body {
   padding: 0;
}

.carousel-control-prev-icon {
	background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%23009be1' viewBox='0 0 8 8'%3E%3Cpath d='M5.25 0l-4 4 4 4 1.5-1.5-2.5-2.5 2.5-2.5-1.5-1.5z'/%3E%3C/svg%3E");
	width: 30px;
	height: 48px;
}
.carousel-control-next-icon {
	background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%23009be1' viewBox='0 0 8 8'%3E%3Cpath d='M2.75 0l-1.5 1.5 2.5 2.5-2.5 2.5 1.5 1.5 4-4-4-4z'/%3E%3C/svg%3E");
	width: 30px;
	height: 48px;
}

    .btn-primary, .btn-primary:hover, .btn-primary:active, .btn-primary:visited {
    background-color: #333399 !important;
    color: #ffff;
}
select{
  color: grey;
}
select option { 
    background-color: white;
    color: grey; 
}
select:invalid { 
  background-color: white;
    color: grey;
}
  </style>
  



  </head>
  <body>
    <div class="parallax-window" data-parallax="scroll" data-image-src="{{ url('img/bg2.jpg') }}">
      <div class="container-fluid">
      <div class="row tm-brand-row">
          <div class="col-lg-4 col-11">
            <img src="{{ url('img/logo.png')}}" style="max-width: 40%">
          </div>
          <div class="col-lg-8 col-1">
            <div class="tm-nav">
              <nav class="navbar navbar-expand-lg navbar-light tm-bg-white-transparent tm-navbar">
                <button class="navbar-toggler" type="button"
                  data-toggle="collapse" data-target="#navbarNav"
                  aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                  <ul class="navbar-nav">
                    <li class="nav-item {{ (request()->is('/')) ? 'active' : '' }}">
                      <div class="tm-nav-link-highlight"></div>
                      <a class="nav-link" href="{{url('/')}}">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item {{ (request()->is('room')) ? 'active' : '' }}">
                      <div class="tm-nav-link-highlight"></div>
                      <a class="nav-link" href="{{url('room')}}">Room</a>
                    </li>
                    <!-- <li class="nav-item {{ (request()->is('facility')) ? 'active' : '' }}">
                      <div class="tm-nav-link-highlight"></div>
                      <a class="nav-link" href="#">Facility</a>
                    </li> -->
                    <li class="nav-item {{ (request()->is('services')) ? 'active' : '' }}">
                      <div class="tm-nav-link-highlight"></div>
                      <a class="nav-link" href="#">Services</a>
                    </li>
                    <li class="nav-item {{ (request()->is('blog')) ? 'active' : '' }}">
                      <div class="tm-nav-link-highlight"></div>
                      <a class="nav-link" href="#">Contact Us</a>
                    </li>
                  </ul>
                </div>
              </nav>
            </div>
          </div>
        </div>

<!-- container start -->

@yield('content')

<!-- container end -->

        <!-- Page footer -->
        <footer class="row mt-5">
          <p class="col-12 text-white text-center tm-copyright-text">
            Copyright &copy; 2022 &nbsp;<a href="#" class="tm-copyright-link">Thomas B P</a>
          </p>
        </footer>
      </div>
      <!-- .container-fluid -->
    </div>

    

    <script src="{{ url('template/js/jquery.min.js') }}"></script>
    <script src="{{ url('template/js/parallax.min.js')}}"></script>
    <script src="{{ url('template/js/bootstrap.min.js')}}"></script>
<script>
  $(document).ready(function ($) {
        $("nav li").click(function ( e ) {
            // e.preventDefault();
            try{
              $("nav li active").removeClass("active"); //Remove any "active" class  
              $("a", this).addClass("active"); //Add "active" class to selected tab 

            }catch(err){
              console.log(err);
            }
         

            // $(activeTab).show(); //Fade in the active content  
        });

      });
  </script>
  </body>
</html>
