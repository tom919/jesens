@extends('layouts.front')

@section('content')



        <section class="row" id="tmHome">
          <div class="col-12 tm-home-container tm-bg-white-transparent p-2">
            <div class="text-grey tm-home-left">
              <p class="text-uppercase tm-slogan">Get Exclusive Deal</p>
              <hr class="tm-home-hr" />
              <h3 class="tm-home-title">Jesen's Inn Kuta</h3>
              <p class="tm-home-text">
                Jl Kubu Anyar Gg. Kresek No. 7, Kuta - Bali
                <br>
                info.jesen1@gmail.com or +6287861333918 (Bu Ari)
              </p>
          
              <a href="{{url('room')}}" class="btn btn-primary"><i class="fa fa-home" aria-hidden="true"></i> &nbsp;Get Room Now</a>
              <!-- <a href="https://wa.me/6287861333918" class="btn btn-primary"><i class="fa fa-whatsapp" aria-hidden="true">
              </i>Whatsapp
              </a> -->
            </div>
          </div>
        </section>

        <!-- Features -->
        <div class="row" id="tmFeatures">
          <div class="col-lg-4">
            <div class="tm-bg-white-transparent tm-feature-box">
            <h3 class="tm-feature-name">Clean Room</h3>
            
            <div class="tm-feature-icon-container">
                <i class="fas fa-3x fa-server"></i>
            </div>

            <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  </p>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="tm-bg-white-transparent tm-feature-box">
                <h3 class="tm-feature-name">Friendly Customer service</h3>

                <div class="tm-feature-icon-container">
                    <i class="fas fa-3x fa-headphones"></i>
                </div>
                <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                </p>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="tm-bg-white-transparent tm-feature-box">
                <h3 class="tm-feature-name">TV Cable</h3>

                <div class="tm-feature-icon-container">
                    <i class="fas fa-3x fa-satellite-dish"></i>
                </div>
                <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                </p>
            </div>
          </div>
        </div>
     




@endsection
