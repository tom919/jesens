@extends('layouts.gridadmin')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-3">Reservation</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
      <div class="room">

      @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

            @if (session('response'))
                <div class="alert alert-success">
                {{ session('response') }}
                </div>
            @endif

         
        <div>
          <form method="post" action="{{url('/reservationadmin/searchresv')}}">
            @csrf
              <div class="form-group">
              <label >Field : </label>
              <select name="field" class="form-control">
              <option value="guest.name">Guest Name</option>
              <option value="bookings.start">Start Date</option>
              <option value="bookings.end">End Date</option>
              <option value="bookings.category">Category</option>
              <option value="bookings.booking_date">Booking Date</option>
                <option value="bookings.booking_code">Booking Code</option>
                <option value="bookings.status">Status</option>
            </select>
                </div>
              <div class="form-group">
              <label >Value : </label>
              <input type="text" class="form-control" name="val">
              </div>
              <div class="form-group">
              <button type="submit" class="btn btn-outline-primary" style="float: right"><i class="fa fa-search"></i>Search</button>
              </div>
          </form>
        </div>
        <br>
        <br>
        <br>
        <br>
        <div >
          <a href="{{url('reservationadmin/new')}}" style="float: right"><button class="btn btn-outline-success mb-2"><i class="fa fa-plus"></i> &nbsp; &nbsp;New Reservation</button></a>
        </div>
    <div>
      @if($data != null)

      <table id="myTable" class="display pt-2">
        <thead>
            <tr>
                <th>Book Code</th>
                <th>Guest Name</th>
                <th>Category</th>
                <th>Resv Date</th>
                <th>Resv Start</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>


  

   @foreach ($data as $dt)


            <tr>
                <td>{{$dt->booking_code}}</td>
                <td>{{$dt->guest_name}}</td>
                <td>{{$dt->booking_category}}</td>
                <td>{{$dt->booking_date}}</td>
                <td>{{$dt->booking_start}}</td>
                <td>{{$dt->booking_status}}</td>
                <td>
            <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-gears"></i>
                              </a>
                                      <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                                      <a class="dropdown-item"  href="{{url('reservationadmin/detail')}}/{{$dt->booking_id}}">Detail</a>
                                      <a class="dropdown-item"  href="{{url('reservationadmin/edit')}}/{{$dt->booking_id}}">Update</a>
                                     
                                      </div>

              </td>
            </tr>

        @endforeach



        </tbody>
    </table>
    @else

    <h3>No Data</h3>


    @endif
</div>
</div>

        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->




@endsection
