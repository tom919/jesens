@extends('layouts.gridadmin')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-5">Room</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">

      <div class="room">

      @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

            
                        <form method="post" action="{{url('/roomadmin/update')}}" enctype="multipart/form-data">
                            @csrf
  
                        <div class="form-group">
                            <label>Room No.</label>
                            <span>{{$room->id}}</span>
                            <input type="hidden" name="id" class="form-control mx-auto" value="{{$room->id}}" required>
                        </div>
                        <div class="form-group" id="roomClass" >
                            <label>Room Class / Type</label>
                            @php
                                $rts=App\Http\Controllers\RoomController::GetRoomType();
                            @endphp
                            <select class="form-control" name="class">
                                <option selected value="{{$room->class}}">{{$room->class}}</option>
                            @foreach($rts as $rt)
                                <option value="{{$rt->id}}">{{$rt->id}} - {{$rt->name}}</option>
                            @endforeach
                            </select>
                            
                        </div>
                     
                        <div class="form-group">
                            <label >Building</label>
                            <select class="form-control" name="building" >
                                <option selected value="{{$room->building}}">{{$room->building}}</option>
                                <option value="North">North</option>
                                <option value="South">South</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label >Floor</label>
                            <select class="form-control" name="floor" >
                                <option selected value="{{$room->floor}}">{{$room->floor}}</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                            </select>
                        </div>
             
                        <div class="form-group">
                            <label >Status</label>
                            <select class="form-control" name="status">
                            @if($room->status=="1")
                                <option value="1" selected>Enabled</option>
                                @else
                                <option value="0" selected>Disabled</option>
                                @endif
                                <option value="1">Enabled</option>
                                <option value="0">Disabled</option>
                            </select>
                        </div>

          
                        <div class="form-group">
                            <button type="submit" class="btn btn-outline-success" style="width: 100%">Save</button>
                        </div>
                        </form>

        </div>



        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>

    </script>


@endsection
