@extends('layouts.gridadmin')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-5">Image</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">

      <div class="room">

      @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

            
                        <form method="post" action="{{url('/imageadmin/update')}}" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" value="{{$images->id}}" name="imagesId">
                            <input type="hidden" value="{{$images->name}}" name="name">
                        <!-- <div class="form-group">
                            <label> Name</label>
                            <input type="text" name="name" class="form-control mx-auto" placeholder="Enter Image Name" value="{{$images->name}}" required>
                        </div> -->
                        <div class="form-group">
                            <label class="col-md-12">Image</label>
                            <img src="{{url('img/upload')}}/{{$images->name}}" style="max-width: 300px">
                            <br>
                            <small>*required</small>
                            <input type="file" class="form-control-file" name="imageFile">
                        </div>
                        <div class="form-group">
                            <label >Category</label>
                            <select class="form-control" name="category" onchange="roomCheck(this);">
                                <option value="{{$images->category}}" selected>{{$images->category}}</option>
                                <option value="GALLERY">Gallery</option>
                                <option value="ROOM">Room</option>
                            </select>
                        </div>
                        <div class="form-group" id="roomRef" style="display: none;">
                            <label>Room Type ID Ref</label>
                            @php
                                $rts=App\Http\Controllers\RoomController::GetRoomType();
                            @endphp
                            <select class="form-control" name="roomRef">
                                <option value="{{$images->room_type}}" selected>Room Reff Id</option>
                            @foreach($rts as $rt)
                                <option value="{{$rt->id}}">{{$rt->id}} - {{$rt->name}}</option>
                            @endforeach
                            </select>
                            
                        </div>

                        <div class="form-group">
                            <label >Status</label>
                            <select class="form-control" name="status">
                                @if($images->status=="1")
                                <option value="1" selected>Enabled</option>
                                @else
                                <option value="0" selected>Disabled</option>
                                @endif
                                <option value="1">Enabled</option>
                                <option value="0">Disabled</option>
                            </select>
                        </div>

          
                        <div class="form-group">
                            <button type="submit" class="btn btn-outline-success" style="width: 100%">Save</button>
                        </div>
                        </form>

        </div>



        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>
function roomCheck(that) {
    if (that.value == "ROOM") {
        document.getElementById("roomRef").style.display = "block";
    } else {
        document.getElementById("roomRef").style.display = "none";
    }
}
    </script>


@endsection
