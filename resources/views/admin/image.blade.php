@extends('layouts.gridadmin')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-5">Image</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">


      <div class="room">

      @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

            @if (session('response'))
                <div class="alert alert-success">
                {{ session('response') }}
                </div>
            @endif

          <a href="{{url('imageadmin/new')}}" style="float: right"><button class="btn btn-outline-success mb-2">New</button></a>


        
      @if($images != null)

<table id="myTable" class="display pt-2">
        <thead>
            <tr>
          <th>Id</th>
          <th>Image</th>
          <th>Category</th>
          <th>Room Type</th>
          <th>Status</th>
                <th></th>
            </tr>
        </thead>
        <tbody>




   @foreach ($images as $im)


            <tr>
          <td>{{$im->id}}</td>
                <td><img src="{{url('/img/upload')}}/{{$im->name}}" style="max-width: 80px"></td>
          <td>{{$im->category}}</td>
          <td>{{$im->room_type}}</td>
          <td>
                              @if($im->status=="1")
                                Enabled
                                @else
                                Disabled
                                @endif
          </td>
                <td>
      <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
      <i class="fa fa-gears"></i>
                        </a>
                                <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item"  href="{{url('/imageadmin/edit')}}/{{$im->id}}">Update</a>
                                  <a class="dropdown-item"  href="{{url('/imageadmin/delete')}}/{{$im->id}}">Delete</a>
                               
                                </div>

        </td>
            </tr>

  @endforeach



        </tbody>
    </table>
@else

<h3>No Data</h3>


@endif








</div>

        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->




@endsection
