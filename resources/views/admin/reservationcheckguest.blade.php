@extends('layouts.gridadmin')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-5">Guest</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">

      <div class="room">

      @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif


            <a href="{{url('/reservationadmin/newguest')}}/{{$data[0]}}/{{$data[1]}}/{{$data[2]}}"><button class="btn  btn-outline-primary" style="float: right">New Guest</button></a>
            <br>
            <h3>search existing guest</h3>
            <form method="post" action="{{url('/reservationadmin/searchguest')}}" enctype="multipart/form-data">
                            @csrf


              <div class="form-group">
              <label >Guest Phone</label>
              <input type="text" class="form-control" name="guest_phone" >
              </div>
              <input type="hidden" name="room_id" value="{{$data[0]}}">
              <input type="hidden" name="start_date" value="{{$data[1]}}">
              <input type="hidden" name="end_date" value="{{$data[2]}}">
              <div class="form-group">
              <button class="btn  btn-outline-success" style="float: right">Search</button>
              </div>

            </form>


            
               

        </div>



        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>

    </script>


@endsection
