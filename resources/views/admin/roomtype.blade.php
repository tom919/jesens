@extends('layouts.gridadmin')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-5">Room Type</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">


      <div class="room">

      @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

            @if (session('response'))
                <div class="alert alert-success">
                {{ session('response') }}
                </div>
            @endif

          <a href="{{url('room/typenew')}}" style="float: right"><button class="btn btn-outline-success mb-2">New Room Type</button></a>


      @if($room_types != null)

      <table id="myTable" class="display pt-2">
        <thead>
            <tr>
                <th>Id</th>
                <th>Type</th>
                <th>Low Price</th>
                <th>High Price</th>
                <th></th>
            </tr>
        </thead>
        <tbody>


  

   @foreach ($room_types as $rt)


            <tr>
                <td>{{$rt->id}}</td>
                <td>{{$rt->name}}</td>
                <td>{{$rt->low_season_price}}</td>
                <td>{{$rt->high_season_price}}</td>
                <td>
            <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-gears"></i>
                              </a>
                                      <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                                      <a class="dropdown-item"  href="{{url('room/typeedit')}}/{{$rt->id}}">Update</a>
                                        <a class="dropdown-item"  href="{{url('room/typedelete')}}/{{$rt->id}}">Delete</a>
                                     
                                      </div>

              </td>
            </tr>

        @endforeach



        </tbody>
    </table>
    @else

    <h3>No Data</h3>


    @endif

</div>

        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->




@endsection
