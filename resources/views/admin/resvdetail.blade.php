@extends('layouts.gridadmin')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">

      <div class="room">

      @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

            <br>
            <h3>Reservation Detail</h3>

            <table class="table table-responsive">
                <tbody>
                <tr>
                    <td>Book Code</td>
                    <td>{{$data['bookingCode']}}
                        <br>
                   
                    </td>
                </tr>
                <tr>
                    <td>Book Date</td>
                    <td>{{$data['bookingDate']}} </td>
                </tr>
                <tr>
                    <td>Reservation Period</td>
                    <td>From : {{$data['startDate']}} To : {{$data['endDate']}}</td>
                </tr>
                <tr>
                    <td>Room No</td>
                    <td>
                       {{$data['roomId']}}    
                    </td>
                </tr>
                <tr>
                    <td>Room Type</td>
                    <td>
                       {{$data['roomType']}}    
                    </td>
                </tr>
                <tr>
                    <td>Name</td>
                    <td>{{$data['guestName']}}</td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td>{{$data['guestAddress']}}</td>
                </tr>
                <tr>
                    <td>Phone</td>
                    <td>{{$data['guestPhone']}}</td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>{{$data['guestEmail']}}</td>
                </tr>
                <tr>
                    <td>Citizenship</td>
                    <td>{{$data['guestCitizenship']}}</td>
                </tr>
                <tr>
                    <td>Payment Method</td>
                    <td>{{$data['paymentMethod']}}</td>
                </tr>
                <tr>
                    <td>Payment Amount</td>
                    <td>IDR
                    @php 
                    echo number_format($data['paymentAmount'],2,',','.');
                    @endphp
                    </td>
                </tr>
                <tr>
                    <td>Payment on Credit</td>
                    <td>{{$data['paymentInsuficient']}}</td>
                </tr>
                <tr>
                    <td>Payment Status</td>
                    <td>{{$data['paymentStatus']}}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <a href="{{url('reservationadmin/edit')}}/{{$data['bookingId']}}"><button class="btn btn-secondary">Edit</button></a>
                    </td>
                </tr>
                </tbody>
            </table>
    
  

            
            
               

        </div>



        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->




@endsection
