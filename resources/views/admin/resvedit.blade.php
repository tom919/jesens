@extends('layouts.gridadmin')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">

      <div class="room">

      @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

            <br>
            <h3>Reservation Update</h3>
            <form method="post" action="{{url('/reservationadmin/update')}}" enctype="multipart/form-data">
                            @csrf
            <input type="hidden" name="bookingId" value="{{$data['bookingId']}}">
            <input type="hidden" name="guestId" value="{{$data['guestId']}}">
            <input type="hidden" name="paymentId" value="{{$data['paymentId']}}">
            <input type="hidden" name="rbId" value="{{$data['rbId']}}">
            <div class="form-group">
                    <label>Book Code</label>
                    <h3>{{$data['bookingCode']}}</h3>
            </div>
            <div class="form-group">
              <label>Booking Category: </label>
              <select name="bookingCategory" class="form-control">
                <option selected value="{{$data['bookingCategory']}}">{{$data['bookingCategory']}}</option>
                <option value="OFFLINE">OFFLINE</option>
              <option value="BOARDING">BOARDING</option>
              <option value="ONLINE">ONLINE</option>
              </select> 
              </div>
                <div class="form-group">
                    <label>Reservation Date</label>
                    <input type="text" name="bookingDate" class="form-control" value="{{$data['bookingDate']}}"> 
                </div>
                <div class="form-group">
                    <label>Reservation Period</label>
                    <br>
                    <label>  From : </label>
                        <input type="text" class="form-control" name="startDate" value="{{$data['startDate']}} ">
                        <br>
                       <label> To : </label> 
                        <input type="text" class="form-control" name="endDate" value="{{$data['endDate']}} ">
                        
            </div>
                <div class="form-group">
                    <label>Room No</label>
                       <input type="text" class="form-control" name="roomId" value="{{$data['roomId']}} ">   
                </div>
                <div class="form-group">
                    <label>Room Type</label>
                       {{$data['roomType']}}    
                </div>
                <div class="form-group">
                    <label>Name</label>
                    
                        <input type="text" class="form-control" name="guestName" value="{{$data['guestName']}}">
                    
                </div>
                <div class="form-group">
                    <label>Address</label>
                    
                        <textarea name="guestAddress" class="form-control">
                        {{$data['guestAddress']}}
                        </textarea>
                    
                </div>
                <div class="form-group">
                    <label>City</label>
                    
                        <input type="text" name="guestCity" class="form-control" value=" {{$data['guestCity']}}">
                    
                </div>
                <div class="form-group">
                    <label>Phone</label>
                    
                        <input type="text" name="guestPhone" class="form-control" value=" {{$data['guestPhone']}}">
                    
                </div>
                <div class="form-group">
                    <label>Email</label>
                    
                        <input type="text" name="guestEmail" class="form-control" value=" {{$data['guestEmail']}}">
                    
                </div>
                <div class="form-group">
                    <label>Nationality</label>
                    
                        <select name="guestNationality" class="form-control">
                            <option selected value="{{$data['guestNationality']}}">{{$data['guestNationality']}}</option>
                        <option value="Indonesian">Indonesian</option>
                        <option value="Non Indonesian">Non Indonesian</option>
                        </select> 
                    
                </div>
                <div class="form-group">
                    <label>Payment Amount</label>
                    IDR
                    <input type="text" class="form-control" name="paymentAmount" value="{{$data['paymentAmount']}}">
                </div>
                <div class="form-group">
                    <label>Payment Method</label>
                <select name="paymentMethod" class="form-control">
                <option selected value="{{$data['paymentMethod']}}">{{$data['paymentMethod']}}</option>
                <option value="Cash">Cash</option>
                <option value="Transfer">Transfer</option>
                </select>
                    </div>
                <div class="form-group">
                    <label>Payment on Credit</label>
                    <input type="text" name="paymentInsuficient" class="form-control" value="{{$data['paymentInsuficient']}}">
                </div>
                <div class="form-group">
              <label>Payment Status: </label>
              <select name="paymentStatus" class="form-control">
                <option selected value="{{$data['paymentStatus']}}">{{$data['paymentStatus']}}</option>
              <option value="PAID">PAID</option>
              <option value="UNPAID">UNPAID</option>
              <option value="CANCEL">CANCEL</option>
              <option value="HALF">HALF<option>
              </select> 
              </div>
              <div class="form-group">
              <label>Booking Status: </label>
              <select name="bookingStatus" class="form-control">
                <option selected value="{{$data['bookingStatus']}}">{{$data['bookingStatus']}}</option>
              <option value="CONFIRMED">CONFIRMED</option>
              <option value="UNCONFIRMED">UNCONFIRMED</option>
              <option value="CANCELED">CANCELED</option>
              </select> 
              </div>
               <div class="form-group">
                    <div class="pull-right"><button type="submit" class="btn btn-outline-success">Update</button></div>
                </div>
            </form>
  

            
            
               

        </div>



        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->




@endsection
