@extends('layouts.gridadmin')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">

      <div class="room">

      @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif


            <a href="{{url('reservationadmin/newguest/')}}"><button class="btn  btn-outline-primary" style="float: right">New Guest</button></a>
            <br>
            <h3>Existing Guest</h3>
            <span>From : {{$data['startDate']}} To: {{$data['endDate']}} For Room No : {{$data['roomId']}} Total Payment : IDR 
                    @php
                      echo number_format($total ,2,',','.'); 
                    @endphp</span>
            <form method="post" action="{{url('/reservationadmin/savexisting')}}" enctype="multipart/form-data">
                            @csrf
          
              <h3><h3>
              <div class="form-group">
              <label >Name : {{$guest->name}}</label>
              
              <input type="hidden" name="guestId" value="{{$guest->id}}">
              <input type="hidden" name="roomId" value="{{$data['roomId']}}">
              <input type="hidden" name="startDate" value="{{$data['startDate']}}">
              <input type="hidden" name="endDate" value="{{$data['endDate']}}">
              </div>
              <div class="form-group">
              <label>Booking Category : </label>
              <select name="g_category" class="form-control">
                <option selected disabled>choose category</option>
              <option value="OFFLINE">OFFLINE</option>
              <option value="BOARDING">BOARDING</option>
              </select> 
              </div>
              <div class="form-group">
              <label >Check In Time : </label>
              <input type="time" name="g_checkin_time" class="form-control">  
              </div>

              <div class="form-group">
              <label>Payment Method : </label>
              <select name="g_payment_method" class="form-control">
                <option selected disabled>choose payment</option>
              <option value="Cash">Cash</option>
              <option value="Transfer">Transfer</option>
              </select> 
              </div>
              <div class="form-group">
              <label>Payment Status: </label>
              <select name="g_payment_status" class="form-control">
                <option selected disabled>choose payment status</option>
              <option value="PAID">PAID</option>
              <option value="UNPAID">UNPAID</option>
              <option value="CANCEL">CANCEL</option>
              <option value="HALF">HALF<option>
              </select> 
              </div>
              <div class="form-group">
              <label >On Credit : </label>
              <input type="text" name="g_payment_insuf" class="form-control">  
              </div>

         
              <div class="form-group">
              <button type="submit" class="btn  btn-outline-success" style="float: right">Submit</button>
              </div>

            </form>


            
               

        </div>



        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>

    </script>


@endsection
