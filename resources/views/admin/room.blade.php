@extends('layouts.gridadmin')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-5">Room</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">


      <div class="room">

      @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

            @if (session('response'))
                <div class="alert alert-success">
                {{ session('response') }}
                </div>
            @endif

          <a href="{{url('roomadmin/new')}}" style="float: right"><button class="btn btn-outline-success mb-2">New Room</button></a>


          <div class="table-responsive">
          <table class="table ">
                <tbody>
                  <tr>
                  <td style="background-color: #f5f5dc">Standart</td>
                  <td style="background-color: #ffbf00">Deluxe</td>
                  <td style="background-color: #cd9575">Superior</td>
                  <td style="background-color: #a4c639">Pool View</td>
                  <td style="background-color: #89cff0">Pool Access</td>
                  <td style="background-color: #ff033e">Disabled</td>
                  </tr>        
                </tbody>
              </table>
          </div>


          <div class="table-responsive">
          <span>Building North</span>
          <table class="table ">
                <tbody>
                  <tr>
                    <td>2nd Floor</td>
                  @php
                      $rs=App\Http\Controllers\RoomController::GetRoomBF('north',2);
                  @endphp
                  @foreach($rs as $room)
                          @php
                           $cl = App\Http\Controllers\RoomController::RoomClassColor($room->class, $room->status);
                          @endphp
                      <td style="background-color: {{$cl}};" class="roomcell"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">{{$room->id}}</a>

                      <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                                      <a class="dropdown-item"  href="{{url('/roomadmin/edit/')}}/{{$room->id}}">Update</a>
                                        <a class="dropdown-item"  href="{{url('/roomadmin/delete/')}}/{{$room->id}}">Delete</a>
                                     
                                      </div>
                    
                    </td>
                  @endforeach
                  </tr>
                  <tr>
                    <td>1st Floor</td>
                  @php
                      $rs=App\Http\Controllers\RoomController::GetRoomBF('north',1);
                  @endphp
                  @foreach($rs as $room)
                          @php
                           $cl = App\Http\Controllers\RoomController::RoomClassColor($room->class, $room->status);
                          @endphp
                          <td style="background-color: {{$cl}};" class="roomcell"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">{{$room->id}}</a>

                            <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item"  href="{{url('/roomadmin/edit/')}}/{{$room->id}}">Update</a>
                                              <a class="dropdown-item"  href="{{url('/roomadmin/delete/')}}/{{$room->id}}">Delete</a>
                                          
                                            </div>

                            </td>
                  @endforeach
                  </tr>           
                </tbody>
              </table>
          </div>

          <br>


          <div class="table-responsive">
          <span>Building South</span>
          <table class="table ">
                <tbody>
                  <tr>
                    <td>3rd Floor</td>
                  @php
                      $rs=App\Http\Controllers\RoomController::GetRoomBF('south',3);
                  @endphp
                  @foreach($rs as $room)
                          @php
                           $cl = App\Http\Controllers\RoomController::RoomClassColor($room->class, $room->status);
                          @endphp
                          <td style="background-color: {{$cl}};" class="roomcell"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">{{$room->id}}</a>

                            <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item"  href="{{url('/roomadmin/edit/')}}/{{$room->id}}">Update</a>
                                              <a class="dropdown-item"  href="{{url('/roomadmin/delete/')}}/{{$room->id}}">Delete</a>
                                          
                                            </div>

                            </td>
                  @endforeach
                  </tr>
                  <tr>
                    <td>2nd Floor</td>
                  @php
                      $rs=App\Http\Controllers\RoomController::GetRoomBF('south',2);
                  @endphp
                  @foreach($rs as $room)
                          @php
                           $cl = App\Http\Controllers\RoomController::RoomClassColor($room->class, $room->status);
                          @endphp
                          <td style="background-color: {{$cl}};" class="roomcell"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">{{$room->id}}</a>

                            <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item"  href="{{url('/roomadmin/edit/')}}/{{$room->id}}">Update</a>
                                              <a class="dropdown-item"  href="{{url('/roomadmin/delete/')}}/{{$room->id}}">Delete</a>
                                          
                                            </div>

                            </td>
                  @endforeach
                  </tr>
                  <tr>
                    <td>1st Floor</td>
                  @php
                      $rs=App\Http\Controllers\RoomController::GetRoomBF('south',1);
                  @endphp
                  @foreach($rs as $room)
                          @php
                           $cl = App\Http\Controllers\RoomController::RoomClassColor($room->class, $room->status);
                          @endphp
                          <td style="background-color: {{$cl}};" class="roomcell"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">{{$room->id}}</a>

                          <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                                          <a class="dropdown-item"  href="{{url('/roomadmin/edit/')}}/{{$room->id}}">Update</a>
                                            <a class="dropdown-item"  href="{{url('/roomadmin/delete/')}}/{{$room->id}}">Delete</a>
                                        
                                          </div>

                          </td>
                  @endforeach
                  </tr>          
                </tbody>
              </table>
          </div>








</div>

        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->




@endsection
