@extends('layouts.gridadmin')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">

      <div class="room">

      <div class="row">
      @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

      </div>



            <br>
            <h3>Booking for New Guest</h3>
            <span>From : {{$data['startDate']}} To: {{$data['endDate']}} For Room No : {{$data['roomId']}} Total Payment : IDR 
                    @php
                      echo number_format($total ,2,',','.'); 
                    @endphp</span>
            <form method="post" action="{{url('/reservationadmin/save')}}" enctype="multipart/form-data">
                            @csrf

              <input type="hidden" name="startDate" value="{{$data['startDate']}}">
              <input type="hidden" name="endDate" value="{{$data['endDate']}}">
              <input type="hidden" name="roomId" value="{{$data['roomId']}}">
              <div class="form-group">
              <label >Name : </label>
              <input type="text" name="g_name" class="form-control">  
              </div>
              <div class="form-group">
              <label >Gov ID / Driving Lisence No : </label>
              <input type="text" name="g_gov_id" class="form-control">  
              </div>
              <div class="form-group">
              <label >Address : </label>
              <textarea name="g_address" class="form-control"></textarea>  
              </div>
              <div class="form-group">
              <label >City : </label>
              <input type="text" name="g_city" class="form-control">  
              </div>
              <div class="form-group">
              <label >Country : </label>
              <input type="text" name="g_country" class="form-control">  
              </div>
              <div class="form-group">
              <label >Phone : </label>
              <input type="text" name="g_phone" class="form-control">  
              </div>
              <div class="form-group">
              <label >Email : </label>
              <input type="email" name="g_email" class="form-control">  
              </div>
              <div class="form-group">
              <label>Nationality : </label>
              <select name="g_nationality" class="form-control">
                <option selected disabled>choose nationality</option>
              <option value="Indonesian">Indonesian</option>
              <option value="Non Indonesian">Non Indonesian</option>
              </select> 
              </div>
              <div class="form-group">
              <label>Booking Category : </label>
              <select name="g_category" class="form-control">
                <option selected disabled>choose category</option>
              <option value="OFFLINE">OFFLINE</option>
              <option value="BOARDING">BOARDING</option>
              </select> 
              </div>
              <div class="form-group">
              <label >Check In Time : </label>
              <input type="time" name="g_checkin_time" class="form-control">  
              </div>

              <div class="form-group">
              <label>Payment Method : </label>
              <select name="g_payment_method" class="form-control">
                <option selected disabled>choose payment</option>
              <option value="Cash">Cash</option>
              <option value="Transfer">Transfer</option>
              </select> 
              </div>
              <div class="form-group">
              <label>Payment Status: </label>
              <select name="g_payment_status" class="form-control">
                <option selected disabled>choose payment status</option>
              <option value="PAID">PAID</option>
              <option value="UNPAID">UNPAID</option>
              <option value="CANCEL">CANCEL</option>
              <option value="HALF">HALF<option>
              </select> 
              </div>
              <div class="form-group">
              <label >Insuficient Payment : </label>
              <input type="text" name="g_payment_insuf" class="form-control">  
              </div>


         
              <div class="form-group">
              <button class="btn  btn-outline-success" style="float: right">Submit</button>
              </div>

            </form>

            
               

        </div>



        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->




@endsection
