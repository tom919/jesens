@extends('layouts.gridadmin')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-5">Room Type</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">

      <div class="room">

      @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

            
                        <form method="post" action="{{url('room/typeupdate')}}" enctype="multipart/form-data">
                            @csrf
                        <div class="form-group">
                            <label >Room Type ID</label>
                            <h3>{{$data->id}}</h3>
                            <input type="hidden" name="room_id" class="form-control mx-auto" placeholder="Enter Room Type Id" value="{{$data->id}}" required>
                        </div>
                        <div class="form-group">
                            <label>Room Description</label>
                            <textarea name="description" class="form-control">{{$data->description}}</textarea>
                        </div>
                        <div class="form-group">
                            <label >Type Name</label>
                            <input type="text" name="name" class="form-control mx-auto" placeholder="Enter Type Name" value="{{$data->name}}" required>
                        </div>
                        <div class="form-group">
                            <label >Low Season Price</label>
                            <input type="number" name="lowPrice" class="form-control mx-auto" placeholder="Enter Low Season Price" value="{{$data->low_season_price}}" required>
                        </div>
                        <div class="form-group">
                            <label >High Season Price</label>
                            <input type="number" name="highPrice" class="form-control mx-auto" placeholder="Enter High Season Price" value="{{$data->high_season_price}}" required>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12">Image 1</label>
                            <input type="file" class="form-control-file" name="image1" >
                            <input type="hidden" name="image1nOld" value="{{$data->image1}}">
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Image 2</label>
                            <input type="file" class="form-control-file" name="image2" >
                            <input type="hidden" name="image2nOld" value="{{$data->image2}}">
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Image 3</label>
                            <input type="file" class="form-control-file" name="image3" >
                            <input type="hidden" name="image3nOld" value="{{$data->image3}}">
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-outline-success" style="width: 100%">Save</button>
                        </div>
                        </form>

        </div>



        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->




@endsection
