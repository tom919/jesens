@extends('layouts.gridadmin')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-5">Searching Room Avaibility</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">

      <div class="room">

      @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

            
                        <form method="post" action="{{url('/reservationadmin/searchroom')}}" enctype="multipart/form-data">
                            @csrf
  
<!-- 
                        <div class="form-group" id="roomClass" >
                            <label>Room Class / Type</label>
                            @php
                                $rts=App\Http\Controllers\RoomController::GetRoomType();
                            @endphp
                            <select class="form-control" name="class" required>
                                <option selected disabled>Room Class</option>
                            @foreach($rts as $rt)
                                <option value="{{$rt->id}}">{{$rt->id}} - {{$rt->name}}</option>
                            @endforeach
                            </select>
                            
                        </div> -->
                     
                        <div class="form-group">
                            <label >Start Date</label>
                            <input type="date" class="form-control" name="start_date" >

                        </div>
                        <div class="form-group">
                            <label >End Date</label>
                            <input type="date" class="form-control" name="end_date" >

                        </div>
             


          
                        <div class="form-group">
                            <button type="submit" class="btn btn-outline-success" style="width: 100%">Search</button>
                        </div>
                        </form>

        </div>



        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>

    </script>


@endsection
