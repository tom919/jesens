@extends('layouts.front')

@section('content')

<section class="row" id="tmServices">
          <div class="col-12">
            <div class="parallax-window tm-services-parallax-header tm-testimonials-parallax-header"
                 data-parallax="scroll"
                 data-z-index="101"
                 data-image-src="img/people.jpg">

                 <div class="tm-bg-black-transparent text-center tm-services-header tm-testimonials-header">
                    <h2 class="text-uppercase tm-services-page-title tm-testimonials-page-title">Search Room</h2>
                    <p class="tm-services-description mb-0 small">
                        New Guest Reservation
                    </p>
                </div>
            </div>
          </div>   
        </section>



        <section class="row tm-contact-row">
            <div class="col-lg-6 tm-contact-col-left">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            

               
            <br>
          
            <form method="post" action="{{url('booking/save')}}" enctype="multipart/form-data" class="tm-bg-black-transparent tm-contact-form">
                            @csrf
              
           
              <div class="form-group">
              <label >Total Payment</label>
              From: {{$data['startDate']}} To: {{$data['endDate']}} 
                </div>
              <div class="form-group">
              <label >Total Payment</label>
              IDR
                    @php 
                    echo number_format($total,2,',','.');
                    @endphp  
              </div>


              <input type="hidden" name="startDate" value="{{$data['startDate']}}">
              <input type="hidden" name="endDate" value="{{$data['endDate']}}">
              <input type="hidden" name="roomId" value="{{$data['roomId']}}">
              <div class="form-group">
              <label >Name : </label>
              <input type="text" name="g_name" class="form-control">  
              </div>
              <div class="form-group">
              <label >Gov ID / Driving Lisence No : </label>
              <input type="text" name="g_gov_id" class="form-control">  
              </div>
              <div class="form-group">
              <label >Address : </label>
              <textarea name="g_address" class="form-control"></textarea>  
              </div>
              <div class="form-group">
              <label >City : </label>
              <input type="text" name="g_city" class="form-control">  
              </div>
              <div class="form-group">
              <label >Country : </label>
              <input type="text" name="g_country" class="form-control">  
              </div>
              <div class="form-group">
              <label >Phone : </label>
              <input type="text" name="g_phone" class="form-control">  
              </div>
              <div class="form-group">
              <label >Email : </label>
              <input type="email" name="g_email" class="form-control">  
              </div>
              <div class="form-group">
              <label>Nationality : </label>
              <select name="g_nationality" class="">
                <option selected disabled>choose nationality</option>
              <option value="Indonesian">Indonesian</option>
              <option value="Non Indonesian">Non Indonesian</option>
              </select> 
              </div>
              <div class="form-group">
              <label>Payment Method : </label>
              <select name="g_payment_method" class="">
                <option selected disabled>choose payment</option>
              <option value="Cash">Cash</option>
              <option value="Transfer">Transfer</option>
              </select> 
</div>
         
              <div class="form-group">
              <button class="btn  btn-outline-primary" style="float: right">Book Now</button>
              </div>

            </form>
                
            </div>
            <div class="col-lg-6 tm-contact-col-right">
                <div class="tm-bg-black-transparent tm-contact-text">
                            
                    
                    <h4 class="tm-service-tab-title text-center">{{$rt->name}}</h3>
  
                    <p class="tm-service-tab-p">
                    <img src="{{url('img/upload')}}/{{$rt->image1}}" style="max-width: 60%; margin-left:20%;" class="img-responsive center-block"></a> 
                    </p>                            
                   
                        <div class="h-100 d-flex align-items-center justify-content-center">
                        <a href="{{url('room/detail')}}/{{$rt->id}}" class="text-center">Detail...</a>
                        </div>
                    </p>
                </div>
            </div>
        </section>



@endsection
