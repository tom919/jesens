@extends('layouts.front')

@section('content')


        <!-- About -->
        <section class="row" id="tmAbout">
          <header class="col-12 tm-about-header">
            <h2 class="text-uppercase text-center text-white tm-about-page-title">Our Room</h2>
            <hr class="tm-about-title-hr">
          </header>
 


          <div class="col-lg-12">
                <div class="tm-bg-black-transparent tm-contact-text">
                <a data-toggle="modal" data-target="#largeModal"><img src="{{url('img/upload')}}/{{$data->image1}}" style="max-width: 30%;  margin-left: auto; margin-right: auto;"></a>          
              <h3 class="tm-about-name">{{$data->name}}</h3>
              <p class="tm-about-description">
               {{$data->description}}
                </div>
        </div>
    
        </section>
            </div>
          </div>
        </section>



  <!-- modal -->
  <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-body">
           <!-- carousel -->
          <div
               id='carouselExampleIndicators'
               class='carousel slide'
               data-ride='carousel'
               >
            <ol class='carousel-indicators'>
              <li
                  data-target='#carouselExampleIndicators'
                  data-slide-to='0'
                  class='active'
                  ></li>
              <li
                  data-target='#carouselExampleIndicators'
                  data-slide-to='1'
                  ></li>
              <li
                  data-target='#carouselExampleIndicators'
                  data-slide-to='2'
                  ></li>
            </ol>
            <div class='carousel-inner'>
              <div class='carousel-item active'>
                <img class='img-size' src="{{url('img/upload')}}/{{$data->image1}}" alt='First slide' />
              </div>
              <div class='carousel-item'>
                <img class='img-size' src="{{url('img/upload')}}/{{$data->image2}}" alt='Second slide' />
              </div>
              <div class='carousel-item'>
                <img class='img-size' src="{{url('img/upload')}}/{{$data->image3}}" alt='Second slide' />
              </div>
            </div>
            <a
               class='carousel-control-prev'
               href='#carouselExampleIndicators'
               role='button'
               data-slide='prev'
               >
              <span class='carousel-control-prev-icon'
                    aria-hidden='true'
                    ></span>
              <span class='sr-only'>Previous</span>
            </a>
            <a
               class='carousel-control-next'
               href='#carouselExampleIndicators'
               role='button'
               data-slide='next'
               >
              <span
                    class='carousel-control-next-icon'
                    aria-hidden='true'
                    ></span>
              <span class='sr-only'>Next</span>
            </a>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

@endsection
