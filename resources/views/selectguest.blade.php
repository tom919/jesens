@extends('layouts.front')

@section('content')

<section class="row" id="tmServices">
          <div class="col-12">
            <div class="parallax-window tm-services-parallax-header tm-testimonials-parallax-header"
                 data-parallax="scroll"
                 data-z-index="101"
                 data-image-src="img/people.jpg">

                 <div class="tm-bg-black-transparent text-center tm-services-header tm-testimonials-header">
                    <h2 class="text-uppercase tm-services-page-title tm-testimonials-page-title">Search Room</h2>
                    <p class="tm-services-description mb-0 small">
                        Guest
                    </p>
                </div>
            </div>
          </div>   
        </section>



        <section class="row tm-contact-row">
            <div class="col-lg-6 tm-contact-col-left">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            
            <div class="tm-bg-black-transparent tm-contact-form" style="height: 15% !important; padding-top: 2% !important;">
                <span>For New Guest click button below</span>
              <a href="{{url('/booking/new')}}/{{$room}}/{{$startDate}}/{{$endDate}}"><button class="btn  btn-sm btn-outline-primary" style="float: right; width: 40% !important;">New</button></a>
            </div>
            <br>
            <form method="post" action="{{url('booking/existing')}}" enctype="multipart/form-data" class="tm-bg-black-transparent tm-contact-form">
                            @csrf
              

              <div class="form-group">
              <span>If you ever use our service, search with your phone number</span>
              </div>
              <div class="form-group">
              <label >Guest Phone</label>
              
              <input type="text" name="guest_phone" class="form-control rounded-0 border-top-0 border-right-0 border-left-0" >
              </div>
              <input type="hidden" name="room_id" value="{{$room}}">
              <input type="hidden" name="start_date" value="{{$startDate}}">
              <input type="hidden" name="end_date" value="{{$endDate}}">
              <div class="form-group">
              <button class="btn  btn-outline-primary" style="float: right">Search</button>
              </div>

            </form>
                
            </div>
            <div class="col-lg-6 tm-contact-col-right">
                <div class="tm-bg-black-transparent tm-contact-text">
                            
                    
                    <h4 class="tm-service-tab-title text-center">{{$rt->name}}</h3>
                    <div class="h-100 d-flex align-items-center justify-content-center">
                    Start From : IDR
                    @php 
                    echo number_format($rt->low_season_price,2,',','.');
                    @endphp  
                    
                    per night
                        </div>
                    <p class="tm-service-tab-p">
                    <img src="{{url('img/upload')}}/{{$rt->image1}}" style="max-width: 60%; margin-left:20%;" class="img-responsive center-block"></a> 
                    </p>                            
                    <div class="h-100 d-flex align-items-center justify-content-center">
                        <a href="{{url('room/detail')}}/{{$rt->id}}" class="text-center">Detail...</a>
                        </div>
                    </p>
                </div>
            </div>
        </section>



@endsection
