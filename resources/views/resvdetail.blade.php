@extends('layouts.front')

@section('content')

<section class="row" id="tmServices">
          <div class="col-12">
            <div class="parallax-window tm-services-parallax-header tm-testimonials-parallax-header"
                 data-parallax="scroll"
                 data-z-index="101"
                 data-image-src="img/people.jpg">

                 <div class="tm-bg-black-transparent text-center tm-services-header tm-testimonials-header">
                    <h2 class="text-uppercase tm-services-page-title tm-testimonials-page-title">Search Room</h2>
                    <p class="tm-services-description mb-0 small">
                        Reservation Detail
                    </p>
                </div>
            </div>
          </div>   
        </section>



        <section class="row tm-contact-row">
            <div class="col-lg-6 tm-contact-col-left">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            

               
           
                
            </div>
            <div class="col-lg-12">
                <div class="tm-bg-black-transparent tm-contact-text">
                 <span>Please Save This Booking Code</span>           
                <h3>Reservation Detail</h3>

<table class="table table-responsive" style="color: white">
    <tbody>
    <tr>
        <td>Book Code</td>
        <td>{{$data['bookingCode']}}
            <br>
       
        </td>
    </tr>
    <tr>
        <td>Book Date</td>
        <td>{{$data['bookingDate']}} </td>
    </tr>
    <tr>
        <td>Reservation Period</td>
        <td>From : {{$data['startDate']}} To : {{$data['endDate']}}</td>
    </tr>
    <tr>
        <td>Room Type</td>
        <td>
           {{$data['roomType']}}    
        </td>
    </tr>
    <tr>
        <td>Name</td>
        <td>{{$data['guestName']}}</td>
    </tr>
    <tr>
        <td>Phone</td>
        <td>{{$data['guestPhone']}}</td>
    </tr>
    <tr>
        <td>Email</td>
        <td>{{$data['guestEmail']}}</td>
    </tr>
    <tr>
        <td>Payment Method</td>
        <td>{{$data['paymentMethod']}}</td>
    </tr>
    <tr>
        <td>Payment Amount</td>
        <td>IDR
        @php 
        echo number_format($data['paymentAmount'],2,',','.');
        @endphp
        </td>
    </tr>
    </tbody>
</table>
                  
                </div>
            </div>
        </section>



@endsection
