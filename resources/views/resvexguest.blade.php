@extends('layouts.front')

@section('content')

<section class="row" id="tmServices">
          <div class="col-12">
            <div class="parallax-window tm-services-parallax-header tm-testimonials-parallax-header"
                 data-parallax="scroll"
                 data-z-index="101"
                 data-image-src="img/people.jpg">

                 <div class="tm-bg-black-transparent text-center tm-services-header tm-testimonials-header">
                    <h2 class="text-uppercase tm-services-page-title tm-testimonials-page-title">Search Room</h2>
                    <p class="tm-services-description mb-0 small">
                        Complete Detail
                    </p>
                </div>
            </div>
          </div>   
        </section>



        <section class="row tm-contact-row">
            <div class="col-lg-6 tm-contact-col-left">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            

               
            <br>
          
            <form method="post" action="{{url('booking/saveexisting')}}" enctype="multipart/form-data" class="tm-bg-black-transparent tm-contact-form">
                            @csrf
              
           

              <div class="form-group">
              <label >Guest Name</label>
              {{$guest->name}}
              </div>
              <div class="form-group">
              <label >Guest Phone</label>
              @php
                      echo App\Http\Controllers\UtilController::MaskString($guest->phone);
                @endphp
              </div>
              <div class="form-group">
              <label >Total Payment</label>
              From: {{$data['startDate']}} To: {{$data['endDate']}} 
                </div>
              <div class="form-group">
              <label >Total Payment</label>
              IDR
                    @php 
                    echo number_format($total,2,',','.');
                    @endphp  
              </div>

              <div class="form-group">
              <label>Payment Method : </label>
              <select name="g_payment_method" class="">
                <option selected disabled>choose payment</option>
              <option value="Cash">Cash</option>
              <option value="Transfer">Transfer</option>
              </select> 
</div>
         
              <div class="form-group">
              <button class="btn  btn-outline-primary" style="float: right">Book Now</button>
              </div>

            </form>
                
            </div>
            <div class="col-lg-6 tm-contact-col-right">
                <div class="tm-bg-black-transparent tm-contact-text">
                            
                    
                    <h4 class="tm-service-tab-title text-center">{{$rt->name}}</h3>
  
                    <p class="tm-service-tab-p">
                    <img src="{{url('img/upload')}}/{{$rt->image1}}" style="max-width: 60%; margin-left:20%;" class="img-responsive center-block"></a> 
                    </p>                            
                    <!-- <p class="mb-0">
                        {{$rt->description}} 
                        <br> -->
                        <div class="h-100 d-flex align-items-center justify-content-center">
                        <a href="{{url('room/detail')}}/{{$rt->id}}" class="text-center">Detail...</a>
                        </div>
                    </p>
                </div>
            </div>
        </section>



@endsection
