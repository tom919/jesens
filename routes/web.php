<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoomController;
use App\Http\Controllers\ImageAdminController;
use App\Http\Controllers\GuestAdminController;
use App\Http\Controllers\ReservationAdminController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/',[FrontController::class, 'Home']);
Route::get('/room',[FrontController::class, 'Room']);
Route::get('/room/detail/{id}',[FrontController::class, 'RoomDetail']);
Route::get('/booking/{id}',[FrontController::class, 'Booking']);
Route::post('/booking/search/',[FrontController::class, 'SearchRoom']);
Route::post('/booking/existing/',[FrontController::class, 'ExistingGuest']);
Route::get('/booking/new/{id}/{startDate}/{endDate}', [FrontController::class, 'NewGuest']);
Route::post('/booking/save', [FrontController::class, 'Save']);
Route::post('/booking/savexisting', [FrontController::class, 'SaveExistingGuest']);

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
// reservation admin
Route::get('/reservationadmin', [ReservationAdminController::class, 'Index']);
Route::get('/reservationadmin/new', [ReservationAdminController::class, 'New']);
Route::post('/reservationadmin/searchroom', [ReservationAdminController::class, 'SearchRoom']);
Route::post('/reservationadmin/searchresv', [ReservationAdminController::class, 'SearchResv']);
Route::get('/reservationadmin/detailresv/{id}/{startDate}/{endDate}', [ReservationAdminController::class, 'CompleteResvCheckGuest']);
Route::get('/reservationadmin/newguest/{id}/{startDate}/{endDate}', [ReservationAdminController::class, 'NewGuest']);
Route::post('/reservationadmin/searchguest', [ReservationAdminController::class, 'SearchGuest']);
Route::post('/reservationadmin/save', [ReservationAdminController::class, 'Save']);
Route::post('/reservationadmin/savexisting', [ReservationAdminController::class, 'SaveExistingGuest']);
Route::get('/reservationadmin/detail/{id}', [ReservationAdminController::class, 'Detail']);
Route::get('/reservationadmin/edit/{id}', [ReservationAdminController::class, 'Edit']);
Route::post('/reservationadmin/update', [ReservationAdminController::class, 'Update']);
Route::get('/reservationadmin/delete/{id}', [ReservationAdminController::class, 'Delete']);
// room admin
Route::get('/guestadmin', [GuestAdminController::class, 'Index']);
// Route::get('/guestadmin/new', [GuestAdminController::class, 'New']);
Route::post('/guestadmin/save', [GuestAdminController::class, 'Save']);
Route::get('/guestadmin/edit/{id}', [GuestAdminController::class, 'Edit']);
Route::post('/guestadmin/update', [GuestAdminController::class, 'Update']);
Route::get('/guestadmin/delete/{id}', [GuestAdminController::class, 'Delete']);
// room admin
Route::get('/roomadmin', [RoomController::class, 'Index']);
Route::get('/roomadmin/new', [RoomController::class, 'New']);
Route::post('/roomadmin/save', [RoomController::class, 'Save']);
Route::get('/roomadmin/edit/{id}', [RoomController::class, 'Edit']);
Route::post('/roomadmin/update', [RoomController::class, 'Update']);
Route::get('/roomadmin/delete/{id}', [RoomController::class, 'Delete']);
//room type admin
Route::get('/room/type', [RoomController::class, 'RoomType']);
Route::get('/room/typenew', [RoomController::class, 'NewRoomType']);
Route::post('/room/typesave', [RoomController::class, 'SaveRoomType']);
Route::get('/room/typeedit/{id}', [RoomController::class, 'EditRoomType']);
Route::post('/room/typeupdate', [RoomController::class, 'UpdateRoomType']);
Route::get('/room/typedelete/{id}', [RoomController::class, 'DeleteRoomType']);
//image admin
Route::get('/imageadmin', [ImageAdminController::class, 'Index']);
Route::get('/imageadmin/new', [ImageAdminController::class, 'New']);
Route::post('/imageadmin/save', [ImageAdminController::class, 'Save']);
Route::get('/imageadmin/edit/{id}', [ImageAdminController::class, 'Edit']);
Route::post('/imageadmin/update', [ImageAdminController::class, 'Update']);
Route::get('/imageadmin/delete/{id}',[ImageAdminController::class, 'Delete']);
